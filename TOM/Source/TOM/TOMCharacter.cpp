// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

/**Header Includes*/
#include "TOMCharacter.h"

/**Game Includes*/
#include "TOMGameInstance.h"
#include "TOMPlayerController.h"

/**Engine Includes*/
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// ATOMCharacter

ATOMCharacter::ATOMCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Default base turn rate
	BaseTurnRate = 45.f;

	// Default Base Look up rate
	BaseLookUpRate = 45.f;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Character moves in the direction of input...	
	GetCharacterMovement()->bOrientRotationToMovement = true;

	// ...at this rotation rate
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);

	// Default jump velocity
	GetCharacterMovement()->JumpZVelocity = 600.f;

	// Default air control limit
	GetCharacterMovement()->AirControl = 0.2f;

	// Initializes the SpringArm Component
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));

	// The distance between the camera and the character
	SpringArmComp->TargetArmLength = 450.0f;

	//Set the socket offset of the SpringArm component.
	SpringArmComp->SocketOffset = FVector(0.0f, 0.0f, 50.0f);

	// Rotate the arm based on the controller
	SpringArmComp->bUsePawnControlRotation = true;

	// Attaches the SpringArm Component to the root component
	SpringArmComp->SetupAttachment(RootComponent);

	// Initializes the Camera Component
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));

	// Camera does not rotate relative to arm
	CameraComp->bUsePawnControlRotation = false;

	// Attaches the Camera Component to the root component but specifically at the end of the boom
	CameraComp->SetupAttachment(SpringArmComp, SpringArmComp->SocketName);

	//Set default to false
	bIsOpened = false;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void ATOMCharacter::BeginPlay()
{
	Super::BeginPlay();
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATOMCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Binds either action or axes mappings for player input

	//Checks to see playerInput Component is valid
	check(PlayerInputComponent);

	/** Set up game play key bindings */
	//Bind action for Jumping
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);

	//Bind action to stop jumping
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//Bind axis to move forward/backward 
	PlayerInputComponent->BindAxis("MoveForward", this, &ATOMCharacter::MoveForward);

	//Bind axis to move left/right
	PlayerInputComponent->BindAxis("MoveRight", this, &ATOMCharacter::MoveRight);

	//Bind axis to interact with objects
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATOMCharacter::Interact);

	//Bind axis to interact with objects
	PlayerInputComponent->BindAction("Map", IE_Pressed, this, &ATOMCharacter::OpenTreasureMap);

	/** We have 2 versions of the rotation bindings to handle different kinds of devices differently */
	//Bind axis to turn camera left/right usually for mouse
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);

	//Bind axis to control camera turn rate left/right usually for joysticks
	PlayerInputComponent->BindAxis("TurnRate", this, &ATOMCharacter::TurnAtRate);

	//Bind axis to turn camera up/down usually for mouse
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	//Bind axis to control camera turn rate up/down usually for joysticks
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATOMCharacter::LookUpAtRate);
}


void ATOMCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATOMCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATOMCharacter::Interact()
{
	OnPlayerPickUp.Broadcast();
}

void ATOMCharacter::OpenTreasureMap()
{
	//Task Not Completed
	auto PlayerController = Cast<ATOMPlayerController>(GetController());
	if (PlayerController)
	{
		PlayerController->ShowUI(TEXT("Map"));
	}
}

void ATOMCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATOMCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
