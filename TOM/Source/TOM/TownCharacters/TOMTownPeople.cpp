// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMTownPeople.h"

/**Game Includes*/
#include "TOMGameInstance.h"
#include "TOMSkySphere.h"
#include "TOMStickMap.h"
#include "TOMMainHUD.h"
#include "TOMWidgetBase.h"

/**Engine Includes*/
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/SphereComponent.h"
#include "EngineUtils.h"
#include "Kismet/KismetMathLibrary.h"
#include "AIController.h"

// Sets default values
ATOMTownPeople::ATOMTownPeople()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Character moves in the direction of input...	
	GetCharacterMovement()->bOrientRotationToMovement = true;

	// ...at this rotation rate
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);

	// Default jump velocity
	GetCharacterMovement()->JumpZVelocity = 600.f;

	// Default air control limit
	GetCharacterMovement()->AirControl = 0.2f;

	// Initializes the Sphere comp
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));

	//Attach tot he root component
	SphereComp->SetupAttachment(RootComponent);

	//Allows the comp to generate overlap events
	SphereComp->SetGenerateOverlapEvents(true);

	//Block all channels for collision
	SphereComp->SetCollisionResponseToAllChannels(ECR_Block);

	//Only checks when a pawn overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	//Ignores collision when a camera overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Camera, ECollisionResponse::ECR_Ignore);

	//Ignores collision when visibility overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Visibility, ECollisionResponse::ECR_Ignore);

	//Show the sphere in game
	SphereComp->SetHiddenInGame(true);

	//Set overlapping default to false
	bIsOverlapping = false;

	//Set Multiplier
	SpeedMultiplier = 100.f;

	//Set default target location
	TargetLocation = FVector(0.0f, 0.0f, 00.f);

}

// Called when the game starts or when spawned
void ATOMTownPeople::BeginPlay()
{
	Super::BeginPlay();

	//Bind the overlapping events to functions that act as the callback when event is triggered
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ATOMTownPeople::OnInteractableBeginOverlap);
	SphereComp->OnComponentEndOverlap.AddDynamic(this, &ATOMTownPeople::OnInteractableEndOverlap);

	//Get the global transforms
	GlobalStartTargetLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);
	
}

void ATOMTownPeople::OnInteractableBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bIsOverlapping)
	{
		//Set overlapping to true
		bIsOverlapping = true;

		//Check Task
		if (!TaskInfo.bIsCompleted)
		{
			for (TActorIterator<ATOMSkySphere> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
				TheSkySphere = *ActorItr;
			}

			//Compare task with Time of Day
			if (TheSkySphere)
			{
				if (TheSkySphere->CheckAndCompareTasks(TaskInfo.CharacterTasks))
				{
					//Task Completed
					auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
					if (GameInstance)
					{
						auto MapWBP = Cast<UTOMStickMap>(GameInstance->FindActiveWidget(TEXT("Map")));
						if (MapWBP)
						{
							if (TaskInfo.MapPart == EMapPart::M_BottomLeft)
							{
								auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
								if (HudWBP)
								{
									if (MapWBP->GetPercantage() >= 3)
									{
										MapWBP->SetBottomLeft();
									}
									else
									{
										FString String("You need to find the missing areas of the map first. When you do, Come back to me");
										HudWBP->SetTaskInfoText(String);
										return;
									}
								}
							}
							else if (TaskInfo.MapPart == EMapPart::M_BottomRight)
							{
								MapWBP->SetBottomRight();
							}
							else if (TaskInfo.MapPart == EMapPart::M_TopLeft)
							{
								MapWBP->SetTopLeft();
							}
							else if (TaskInfo.MapPart == EMapPart::M_TopRight)
							{
								MapWBP->SetTopRight();
							}

							//Update the map percentage status
							MapWBP->SetPercentageText();

							OnTaskCompleted();
						}
					}
				}
				else
				{
					OnTaskUnCompleted();
				}
			}
		}

		UE_LOG(LogTemp, Warning, TEXT("Interacting with Object"))
	}
}

void ATOMTownPeople::OnInteractableEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (bIsOverlapping)
	{
		//Set overlapping to true
		bIsOverlapping = false;
	}
}

void ATOMTownPeople::OnTaskCompleted()
{
	//Task Not Completed
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
		if (HudWBP)
		{
			HudWBP->SetTaskInfoText(TaskInfo.CompletedTaskText);
			TaskInfo.bIsCompleted = true;
		}
	}
}

void ATOMTownPeople::MoveCharacter(float DeltaSeconds)
{
	FVector Location = GetActorLocation();

	PathLength = UKismetMathLibrary::Subtract_VectorVector(GlobalTargetLocation, GlobalStartTargetLocation).Size();
	PathTravelled = UKismetMathLibrary::Subtract_VectorVector(Location, GlobalStartTargetLocation).Size();

	if (PathTravelled >= PathLength)
	{
		FVector Swap = GlobalStartTargetLocation;
		GlobalStartTargetLocation = GlobalTargetLocation;
		GlobalTargetLocation = Swap;
	}

	Location += SpeedMultiplier * DeltaSeconds * PathDirection;
	CalculateDirection(GlobalStartTargetLocation, GlobalTargetLocation);
	//AddMovementInput(Location, 1.0F);
}

void ATOMTownPeople::OnTaskUnCompleted()
{
	//Task Not Completed
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
		if (HudWBP)
		{
			HudWBP->SetTaskInfoText(TaskInfo.UnCompletedTaskText);
			TaskInfo.bIsCompleted = false;
		}
	}
}

void ATOMTownPeople::CalculateDirection(FVector Start, FVector End)
{
	PathDirection = UKismetMathLibrary::Subtract_VectorVector(End, Start).GetSafeNormal();
}

void ATOMTownPeople::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Move character
	MoveCharacter(DeltaSeconds);
}