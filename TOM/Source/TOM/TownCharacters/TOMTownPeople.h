// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

/** Auto Generated Includes */
#include "TOMTownPeople.generated.h"

UCLASS()
class TOM_API ATOMTownPeople : public ACharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	ATOMTownPeople();

protected:

	/** Sphere component for the actor */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TownPeople", meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SphereComp = nullptr;


	/** Struct that contains information about the characters task */
	UPROPERTY(EditDefaultsOnly, Category = "Tasks", meta = (AllowPrivateAccess = "true"))
	FTasks TaskInfo;


	/** Is true if pawn is overlapping this interactable object otherwise false */
	bool bIsOverlapping = false;


	/** Pointer to the Character */
	class ATOMCharacter* MainCharacter = nullptr;


	/** Ptr to SkySphere Object */
	UPROPERTY()
	class ATOMSkySphere* TheSkySphere;


	/** How fast the character walks */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TownPeople")
	float SpeedMultiplier;


	/** Target location for the character */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TownPeople", Meta = (MakeEditWidget = true))
	FVector TargetLocation;


	/** Global start Target location for the character */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TownPeople")
	FVector GlobalStartTargetLocation;


	/** Global Target location for the character */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TownPeople")
	FVector GlobalTargetLocation;

	
	/** Direction towards the target */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TownPeople")
	FVector PathDirection;

	/** Length of path to target */
	float PathLength;

	/** Distance traveled by character */
	float PathTravelled;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	/**
	 *	Event called when something starts to overlaps this component, for example a player walking into a trigger.
	 *	For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.
	 *
	 *	@note Both this component and the other one must have GetGenerateOverlapEvents() set to true to generate overlap events.
	 *	@note When receiving an overlap from another object's movement, the directions of 'Hit.Normal' and 'Hit.ImpactNormal'
	 *	will be adjusted to indicate force from the other object against this object.
	 */
	UFUNCTION()
	void OnInteractableBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	/**
	 *	Event called when something stops overlapping this component
	 *	@note Both this component and the other one must have GetGenerateOverlapEvents() set to true to generate overlap events.
	 */
	UFUNCTION()
	void OnInteractableEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	/**
	 *	Function that is called when task is completed
	 */
	UFUNCTION()
	void OnTaskCompleted();


	/**
	 *	Function that moves the character
	 */
	void MoveCharacter(float DeltaSeconds);

	/**
	 *	Function that is called when task is uncompleted
	 */
	UFUNCTION()
	void OnTaskUnCompleted();


	/**
	 *	Function that calculates the direction to the target
	 *  @param  Start  Start Location
	 *  @param  End  End Location
	 */
	void CalculateDirection(FVector Start, FVector End);

public:	

	virtual void Tick(float DeltaSeconds) override;

};
