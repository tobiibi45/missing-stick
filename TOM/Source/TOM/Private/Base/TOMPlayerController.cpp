// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMPlayerController.h"

/**Game Includes*/
#include "TOMCharacter.h"
#include "TOMInteractables.h"
#include "TOMGameInstance.h"
#include "TOMMainHUD.h"

ATOMPlayerController::ATOMPlayerController()
{
	bIsOpened = false;
}


void ATOMPlayerController::BeginPlay()
{
	Super::BeginPlay();

	pCharacter = Cast<ATOMCharacter>(GetCharacter());

	pGameInstance = Cast<UTOMGameInstance>(GetGameInstance());

	//Populate array with default info for inventory
	for (auto i = 0; i < 5; ++i)
	{
		//Set the interactable struct
		const FInteractables Item;

		//Populate with empty structs
		FInventoryObjects.Add(Item);
	}
}


int32 ATOMPlayerController::FindEmptySlot() const
{
	for (auto i = 0; i < FInventoryObjects.Num(); ++i)
	{
		if (FInventoryObjects[i].InteractableClass == nullptr)
		{
			//Print a log
			UE_LOG(LogTemp, Warning, TEXT("Empty Slot index: [%d]"), i);
			return i;
		}
	}

	return INDEX_NONE;
}

bool ATOMPlayerController::IsStickFound() const
{
	for (auto i = 0; i < FInventoryObjects.Num(); ++i)
	{
		if (FInventoryObjects[i].InteractableClass != nullptr)
		{
			if (FInventoryObjects[i].InteractableType == EItneractableType::I_Item)
			{
				return true;
			}
		}
	}

	return false;
}

void ATOMPlayerController::PlayerInteract(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr)
{
	auto result = FInventoryObjects.Find(InteractableInfo);

	if (result == INDEX_NONE)
	{
		if (InteractableInfo.InteractableType == EItneractableType::I_Clock)
		{
			return;
		}

		//Find an empty slot index
		auto index = FindEmptySlot();
		if (index != INDEX_NONE)
		{
			//Set the interactable struct
			FInteractables Item = InteractableInfo;

			//Set the boolean saying the item has been picked
			Item.bHasBeenPicked = true;

			//Set the item index
			Item.Index = index;

			//Set the interactable ptr
			Item.InteractableClass = Interactableptr;

			//Delete actor and empty temp array
			OnPickUp(Item.InteractableClass, Item);

			//Add to inventory
			AddItemToArray(Item, Item.Index);

			//Print a log
			UE_LOG(LogTemp, Warning, TEXT("Slot where new item was inserted: [%d]"), Item.Index);

		}
	}
}

void ATOMPlayerController::ShowUI(const FString& UiName)
{
	//Get the game instance
	if (pGameInstance)
	{
		if (bIsOpened)
		{

			//Create and open the UI
			pGameInstance->HideUI(UiName);

			//Set is opened
			bIsOpened = false;

			return;
		}
		else
		{

			//Create and open the UI
			pGameInstance->ShowUI(UiName);

			//Set is opened
			bIsOpened = true;

			return;

		}
	}
}

void ATOMPlayerController::CloseUI(const FString& UiName)
{
	//Get the game instance
	if (pGameInstance)
	{
		//Closes the active UI
		pGameInstance->RemoveWidget(UiName);

		//set opened to false
		bIsOpened = false;
	}
}

void ATOMPlayerController::ReceiveInfo(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr)
{
	//Create and initialize a tuple
	FTEMPITEMS Tuple(InteractableInfo.InteractableType, Interactableptr);

	const auto result = FOverlappedItems.Find(Tuple);
	if (result == INDEX_NONE)
	{
		//Add to temp array
		FOverlappedItems.Add(Tuple);
	}
}

void ATOMPlayerController::SetIsOpened(bool bIsOpen)
{
	bIsOpened = bIsOpen;
}

void ATOMPlayerController::DeleteInfo(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr)
{
	//Create and initialize a tuple
	FTEMPITEMS Tuple(InteractableInfo.InteractableType, Interactableptr);

	const auto result = FOverlappedItems.Find(Tuple);
	if (result != INDEX_NONE)
	{
		//Remove from temp array
		FOverlappedItems.RemoveAtSwap(result);
	}
}

void ATOMPlayerController::OnPickUp(class ATOMInteractables* Interactableptr, const FInteractables& InteractableInfo)
{
	//Create and initialize a tuple
	FTEMPITEMS Tuple(InteractableInfo.InteractableType, Interactableptr);

	for (auto i = 0; i < FOverlappedItems.Num(); ++i)
	{
		if (FOverlappedItems[i].Value == Interactableptr)
		{
			FOverlappedItems.RemoveAtSwap(i);
			//Interactableptr->Destroy();
		}
	}
}

void ATOMPlayerController::AddItemToArray(const FInteractables& InteractableInfo, int32 Index)
{
	//Adding to the inventory array
	for (auto i = 0; i < FInventoryObjects.Num(); ++i)
	{
		if (i == Index)
		{
			//Fill the item info
			FInventoryObjects[i] = InteractableInfo;
		}
	}
}
