// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMGameInstance.h"

/*Game Include*/
#include "TOMMainHUD.h"
#include "TOMWidgetBase.h"
#include "TOMBeginWidget.h"
#include "TOMStickMap.h"
#include "TOMDayClock.h"
#include "TOMSkySphere.h"

/*Engine Include*/
#include "Engine/Engine.h"
#include "ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Tuple.h"
#include "Components/AudioComponent.h"

UTOMGameInstance::UTOMGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	bMainHudOpened(false)
{
	//Registered the Main HUD
	ConstructorHelpers::FClassFinder<UUserWidget> MainHudWBP(TEXT("/Game/Widgets/WBP_HUD"));
	if (MainHudWBP.Class != NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("Setting MAIN HUD wbp class"));

		//Create a tuple and add this to the map
		WIDGETS HudTuple(TEXT("MainHUD"), MainHudWBP.Class);
		WidgetArray.Add(HudTuple);
	}

	//Registered the Begin Game widget
	ConstructorHelpers::FClassFinder<UUserWidget> WBPWidgetBegin(TEXT("/Game/Widgets/WBP_GameStarts"));
	if (WBPWidgetBegin.Class != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Setting Game Begin wbp class"));

		//Create a tuple and add this to the map
		WIDGETS GameStartTuple(TEXT("GameBegin"), WBPWidgetBegin.Class);
		WidgetArray.Add(GameStartTuple);
	}

	//Registered the End Game widget
	ConstructorHelpers::FClassFinder<UUserWidget> WBPWidgetEnd(TEXT("/Game/Widgets/WBP_GameOver"));
	if (WBPWidgetEnd.Class != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Setting Game End wbp class"));

		//Create a tuple and add this to the map
		WIDGETS EndTuple(TEXT("GameEnd"), WBPWidgetEnd.Class);
		WidgetArray.Add(EndTuple);
	}

	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundCue(TEXT("/Game/SoulCity/Sound/Cue/AmbientLooping/Wind_Breeze01_Cue"));
	if (SoundCue.Succeeded())
	{
		//Set the parent material id succeeded
		NormalMusic = SoundCue.Object;
	}
}

void UTOMGameInstance::Init()
{
	Super::Init();

}

void UTOMGameInstance::Shutdown()
{
	Super::Shutdown();

	//Empty all the arrays
	WidgetArray.Empty();
	ActiveWidgets.Empty();
}

class UTOMWidgetBase* UTOMGameInstance::CreateNewWidget(TSubclassOf<class UUserWidget>& Widget, const FString& WidgetName)
{
	if (ensure(Widget))
	{
		auto NewWidget = CreateWidget<UTOMWidgetBase>(this, Widget);
		NewWidget->Setup();

		TTuple< const FString, class UTOMWidgetBase* > TheTuple(WidgetName, NewWidget);
		ActiveWidgets.Add(TheTuple);

		return NewWidget;
	}

	return nullptr;
}


void UTOMGameInstance::RemoveWidget(const FString& Name)
{
	UE_LOG(LogTemp, Warning, TEXT("Removing Widget from viewport [%s]"), *Name);

	auto Widget = FindActiveWidget(Name);

	if (ensure(Widget))
	{
		Widget->CleanUp();
	}
}

void UTOMGameInstance::OnGameCompleted()
{
	//Load Game Over Widget
	auto TheGameEnd = FindWidgetSubClass(TEXT("GameEnd"));
	if (TheGameEnd)
	{

		CreateNewWidget(TheGameEnd, TEXT("GameEnd"));

		if (AudioComponent)
		{
			AudioComponent->SetActive(false);
		}
	}
}

void UTOMGameInstance::OnGameBegins()
{
	auto TheMainHUD = FindWidgetSubClass(TEXT("MainHUD"));
	if (TheMainHUD)
	{
		CreateNewWidget(TheMainHUD, TEXT("MainHUD"));

		//Set Game start bool to true
		bGameHasStarted = true;

		auto HUD = Cast<UTOMMainHUD>(FindActiveWidget(TEXT("MainHUD")));
		if (HUD)
		{
			if (HUD->GetMapRef())
			{
				TTuple< const FString, class UTOMWidgetBase* > TheTuple(TEXT("Map"), HUD->GetMapRef());
				ActiveWidgets.Add(TheTuple);
			}

			if (HUD->GetDayTimeRef())
			{
				TTuple< const FString, class UTOMWidgetBase* > TheTuple(TEXT("TimeOfDay"), HUD->GetDayTimeRef());
				ActiveWidgets.Add(TheTuple);
			}
		}

		AudioComponent = UGameplayStatics::SpawnSound2D(GetWorld(), NormalMusic);
		if (AudioComponent)
		{
			
			AudioComponent->SetActive(true);
		}
		
		//Pick random time of day
		for (TActorIterator<ATOMSkySphere> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
			auto Skysphere = *ActorItr;
			if (Skysphere)
			{
				Skysphere->StartWithRandomDay();
			}
		}
	}

}

bool UTOMGameInstance::GetIsMusicPlaying()
{
	if (AudioComponent)
	{
		return AudioComponent->bIsPaused;
	}

	return false;
}

void UTOMGameInstance::SetMusicPlaying(bool bActive)
{
	if (AudioComponent)
	{
		AudioComponent->SetPaused(bActive);
	}
}

void UTOMGameInstance::ShowUI(const FString& Name)
{
	auto StickMap = FindActiveWidget(Name);
	if (StickMap)
	{
		StickMap->SetVisibility(ESlateVisibility::Visible);
		auto HUD = Cast<UTOMMainHUD>(FindActiveWidget(TEXT("MainHUD")));
		if (HUD)
		{
			HUD->SetInputMode(true);
		}
	}
}

void UTOMGameInstance::HideUI(const FString& Name)
{
	auto StickMap = FindActiveWidget(Name);
	if (StickMap)
	{
		StickMap->SetVisibility(ESlateVisibility::Hidden);
		auto HUD = Cast<UTOMMainHUD>(FindActiveWidget(TEXT("MainHUD")));
		if (HUD)
		{
			HUD->SetInputMode(false);
		}
	}
}


TSubclassOf<class UUserWidget>& UTOMGameInstance::FindWidgetSubClass(const FString &Name)
{
	auto FoundClass = WidgetArray.Find(Name);

	if (FoundClass != nullptr)
	{
		return *FoundClass;
	}

	return *FoundClass;
}

class UTOMWidgetBase* UTOMGameInstance::FindActiveWidget(const FString& Name)
{
	if (!bGameHasStarted)
	{
		return nullptr;
	}

	auto FoundClass = ActiveWidgets.Find(Name);

	if (FoundClass != nullptr)
	{
		return *FoundClass;
	}

	return *FoundClass;
}