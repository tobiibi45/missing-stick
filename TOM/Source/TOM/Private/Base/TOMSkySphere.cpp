// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMSkySphere.h"

/**Game Includes*/
#include "TOMGameInstance.h"
#include "TOMMainHUD.h"

/**Engine Includes*/
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/DirectionalLight.h"
#include "Curves/CurveLinearColor.h"
#include "Components/DirectionalLightComponent.h"
#include "ConstructorHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "EngineUtils.h"
#include "Engine/Texture2D.h"

// Sets default values
ATOMSkySphere::ATOMSkySphere()
	: bRefreshMaterial(false),
	bDetermineSunPosition(true),
	bUpdateSunPosition(false),
	LightDirectionName("Light direction"),
	SunColorName("Sun color"),
	HorizonColorName("Horizon color"),
	ZenithColorName("Zenith color"),
	CloudColorName("Cloud color"),
	HorizonFallOffName("Horizon fallOff"),
	OverallColorName("Overall color"),
	CloudSpeedName("Cloud speed"),
	SunBrightnessName("Sun brightness"),
	SunHeightName("Sun height"),
	CloudOpacityName("Cloud opacity"),
	StarsBrightnessName("Stars brightness"),
	SunHeight(0.0f),
	HorizonFallOff(3.0f),
	SunBrightness(50.0f),
	CloudSpeed(1.0f),
	CloudOpacity(0.7f),
	StarsBrightness(0.1f),
	ZenithColor(0.034046f, 0.109247f, 0.295f),
	HorizonColor(1.979559f, 2.586644f, 3.0f),
	CloudColor(0.855778f, 0.91902f, 1.0f),
	OverallColor(1.0f, 1.0f, 1.0f),
	DayNightCycleSpeed(5.0f),
	TargetSunHeight(0.0f),
	CurrentTimeOfDay(ETimeOfDay::T_Morning),
	NextTimeOfDay(ETimeOfDay::T_None),
	WidgetTimeDay(ETimeOfDay::T_None)
{

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initializes the Scene Component
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));

	// Set the scene component as the root component
	RootComponent = SceneComp;

	// Initializes the Mesh Component
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	
	//Set the MeshComp to Movable. Basically saying the sun can be moved
	MeshComp->SetMobility(EComponentMobility::Movable);

	//Attach he MeshComp to the root component
	MeshComp->SetupAttachment(RootComponent);

	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<UCurveLinearColor> HorizonCurveObj(TEXT("/Engine/EngineSky/C_Sky_Horizon_Color"));
	if (HorizonCurveObj.Succeeded())
	{
		//Set the parent material id succeeded
		HorizonCurveColor = HorizonCurveObj.Object;
	}

	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<UCurveLinearColor> CloudCurveObj(TEXT("/Engine/EngineSky/C_Sky_Cloud_Color"));
	if (CloudCurveObj.Succeeded())
	{
		//Set the parent material id succeeded
		CloudCurveColor = CloudCurveObj.Object;
	}

	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<UCurveLinearColor> ZenithCurveObj(TEXT("/Engine/EngineSky/C_Sky_Zenith_Color"));
	if (ZenithCurveObj.Succeeded())
	{
		//Set the parent material id succeeded
		ZenithCurveColor = ZenithCurveObj.Object;
	}

	// Finds and loads the parent material
	static ConstructorHelpers::FObjectFinder<UMaterial> SkyMaterial(TEXT("/Game/Blueprint/M_Sky"));
	if (SkyMaterial.Succeeded())
	{
		//Set the parent material id succeeded
		ParentMaterial = SkyMaterial.Object;
	}

	// Finds and loads the static mesh
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SkySphere(TEXT("/Engine/EngineSky/SM_SkySphere"));
	if (SkySphere.Succeeded())
	{
		//Set the parent material id succeeded
		MeshComp->SetStaticMesh(SkySphere.Object);
	}

	// Finds and loads the parent material
	static ConstructorHelpers::FObjectFinder<UTexture2D> MorningObj(TEXT("/Game/Assets/Icons/Sun"));
	if (MorningObj.Succeeded())
	{
		//Set the parent material id succeeded
		MorningImage = MorningObj.Object;
	}

	// Finds and loads the parent material
	static ConstructorHelpers::FObjectFinder<UTexture2D> AfternoonObj(TEXT("/Game/Assets/Icons/Afternoon"));
	if (AfternoonObj.Succeeded())
	{
		//Set the parent material id succeeded
		AfternoonImage = AfternoonObj.Object;
	}

	// Finds and loads the parent material
	static ConstructorHelpers::FObjectFinder<UTexture2D> EveningObj(TEXT("/Game/Assets/Icons/Moon"));
	if (EveningObj.Succeeded())
	{
		//Set the parent material id succeeded
		EveningImage = EveningObj.Object;
	}

}

// Called when the game starts or when spawned
void ATOMSkySphere::BeginPlay()
{
	Super::BeginPlay();
	
	for (TActorIterator<ADirectionalLight> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
		DirectionLight = *ActorItr;
	}

	if (!MaterialInst)
	{
		MaterialInst = UMaterialInstanceDynamic::Create(ParentMaterial, MeshComp);
		if (MaterialInst)
		{
			// Set the saved material instance to the mesh comp
			MeshComp->SetMaterial(0, MaterialInst);
		}
	}

	if (!GameInstance)
	{
		GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	}

	//Refresh the Sky Material
	RefreshMaterial();

}

void ATOMSkySphere::UpdateSunDirection()
{
	if (DirectionLight)
	{
		//Calculate the sun height
		CalculateSunHeight(DirectionLight);
	}

	if (MaterialInst)
	{
		//Set the vector value for the Horizon color
		MaterialInst->SetVectorParameterValue(HorizonColorName, GetClampedLinearColor(HorizonCurveColor, SunHeight));

		//Set the vector value for the Zenith color
		MaterialInst->SetVectorParameterValue(ZenithColorName, GetClampedLinearColor(ZenithCurveColor, SunHeight));

		//Set the vector value for the Cloud color
		MaterialInst->SetVectorParameterValue(CloudColorName, GetClampedLinearColor(CloudCurveColor, SunHeight));

		//Set the Scalar value for the Horizon Fall off color
		MaterialInst->SetScalarParameterValue(HorizonFallOffName, UKismetMathLibrary::Lerp(3.0f, 8.0f, UKismetMathLibrary::Abs(SunHeight)));

		//If sun height is negative, this is used to blend the stars
		auto SelectedFloatVal = UKismetMathLibrary::SelectFloat(UKismetMathLibrary::Abs(SunHeight), 0.0f, SunHeight < 0.0f);

		//Set the Scalar value for the Sun height. 
		MaterialInst->SetScalarParameterValue(SunHeightName, SelectedFloatVal);

	}
}

void ATOMSkySphere::RefreshMaterial()
{
	//Calculate the Light direction	
	CalculateSunHeight(DirectionLight);

	//Determine the colour of the sky
	DetermineColorsBySunPosition(bDetermineSunPosition);

	if (MaterialInst)
	{
		//Set the Scalar value for the Cloud Speed
		MaterialInst->SetScalarParameterValue(CloudSpeedName, CloudSpeed);

		//Set the Scalar value for the Sun brightness
		MaterialInst->SetScalarParameterValue(SunBrightnessName, SunBrightness);

		//If sun height is negative, this is used to blend the stars
		auto SelectedFloatVal = UKismetMathLibrary::SelectFloat(UKismetMathLibrary::Abs(SunHeight), 0.0f, SunHeight < 0.0f);

		//Set the Scalar value for the Sun height. 
		MaterialInst->SetScalarParameterValue(SunHeightName, SelectedFloatVal);

		//Set the Scalar value for the Cloud Opacity
		MaterialInst->SetScalarParameterValue(CloudOpacityName, CloudOpacity);

		//Set the Scalar value for the Stars brightness
		MaterialInst->SetScalarParameterValue(StarsBrightnessName, StarsBrightness);
	}

}

FLinearColor ATOMSkySphere::GetClampedLinearColor(class UCurveLinearColor* LinearCurve, float InValue)
{
	if (LinearCurve)
	{
		return LinearCurve->GetClampedLinearColorValue(InValue);
	}

	return FLinearColor();
}

void ATOMSkySphere::CalculateLightDiretion(class ADirectionalLight* Light)
{
	//Calculate the sun height
	CalculateSunHeight(Light);

	if (!Light)
	{
		//Get the value mapped from one range to another using the sun height
		float PitchVal = UKismetMathLibrary::MapRangeUnclamped(SunHeight, -1.0f, 1.0f, 90.0f, -90.0f);

		//Creates a rotator from the values supplied
		auto Rotaion = UKismetMathLibrary::MakeRotator(0.0f, PitchVal, this->GetActorRotation().Yaw);

		//Get the rotation X Vector
		auto XVetor = UKismetMathLibrary::Conv_RotatorToVector(Rotaion);

		//Get the Light direction  linear color from the vector
		auto LightDirectionLinearColor = UKismetMathLibrary::Conv_VectorToLinearColor(XVetor);

		if (MaterialInst)
		{
			//Set the vector value for the light direction
			MaterialInst->SetVectorParameterValue(LightDirectionName, LightDirectionLinearColor);
		}

		//Get the Colour alpha from the sun height + 0.2
		auto ColorAlpha = FMath::Clamp((SunHeight + 0.2f), 0.0f, 1.0f);

		//Linearly interpolates between two Linear colors
		auto LerpValue = UKismetMathLibrary::LinearColorLerp(FLinearColor(1.0f, 0.221f, 0.04f), FLinearColor(0.954f, 0.901f, 0.74412f), ColorAlpha);

		if (MaterialInst)
		{
			//Set the vector value for the Sun color
			MaterialInst->SetVectorParameterValue(SunColorName, LerpValue);
		}
	}
}

void ATOMSkySphere::CalculateSunHeight(class ADirectionalLight* Light)
{
	if (Light)
	{
		//Get the rotation X Vector
		auto XVetor = UKismetMathLibrary::Conv_RotatorToVector(Light->GetActorRotation());

		//Get the Light direction  linear color from the vector
		auto LightDirectionLinearColor = UKismetMathLibrary::Conv_VectorToLinearColor(XVetor);

		if (MaterialInst)
		{
			//Set the vector value for the light direction
			MaterialInst->SetVectorParameterValue(LightDirectionName, LightDirectionLinearColor);
		}

		//Get the Sun direction  linear color from the vector
		auto SunDirectionLinearColor = UKismetMathLibrary::Conv_ColorToLinearColor(Light->GetLightComponent()->LightColor);

		if (MaterialInst)
		{
			//Set the vector value for the Sun color
			MaterialInst->SetVectorParameterValue(SunColorName, SunDirectionLinearColor);
		}

		//Get the value mapped from one range to another using the direction light rotation and then sets the result to the sun height
		SunHeight = UKismetMathLibrary::MapRangeUnclamped(Light->GetActorRotation().Pitch, 0.0f, -90.0f, 0.0f, 1.0f);
	}
}

void ATOMSkySphere::DetermineColorsBySunPosition(bool bDetermineColors)
{
	if (bDetermineColors)
	{
		if (MaterialInst)
		{
			//Set the vector value for the Horizon color
			MaterialInst->SetVectorParameterValue(HorizonColorName, GetClampedLinearColor(HorizonCurveColor, SunHeight));

			//Set the vector value for the Zenith color
			MaterialInst->SetVectorParameterValue(ZenithColorName, GetClampedLinearColor(ZenithCurveColor, SunHeight));

			//Set the vector value for the Cloud color
			MaterialInst->SetVectorParameterValue(CloudColorName, GetClampedLinearColor(CloudCurveColor, SunHeight));

			//Set the Scalar value for the Horizon Fall off color
			MaterialInst->SetScalarParameterValue(HorizonFallOffName, UKismetMathLibrary::Lerp(3.0f, 7.0f, UKismetMathLibrary::Abs(SunHeight)));
		}
	}

	if (!bDetermineColors)
	{
		if (MaterialInst)
		{
			//Set the vector value for the Horizon color
			MaterialInst->SetVectorParameterValue(HorizonColorName, HorizonColor);

			//Set the vector value for the Zenith color
			MaterialInst->SetVectorParameterValue(ZenithColorName, ZenithColor);

			//Set the vector value for the Overall color
			MaterialInst->SetVectorParameterValue(OverallColorName, OverallColor);

			//Set the vector value for the Cloud color
			MaterialInst->SetVectorParameterValue(CloudColorName, CloudColor);

			//Set the Scalar value for the Horizon Fall off color
			MaterialInst->SetScalarParameterValue(HorizonFallOffName, HorizonFallOff);
		}
	}
}

// Called every frame
void ATOMSkySphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Update the day
	UpdateDay(DeltaTime, DayNightCycleSpeed);
}

void ATOMSkySphere::SetUpdateDayCycle(bool bUpdateDay, float Speed)
{
	bUpdateSunPosition = bUpdateDay;
	DayNightCycleSpeed = Speed;
}

void ATOMSkySphere::SetTimeOfDay(ETimeOfDay TheDay, float Speed /*= 30.0f*/)
{
	switch (TheDay)
	{
	case ETimeOfDay::T_None:
		return;
		break;
	case ETimeOfDay::T_Morning:
		CurrentTimeOfDay = TheDay;
		TargetSunHeight = FMath::RandRange(0.1f, 0.5f);
		break;
	case ETimeOfDay::T_Afternoon:
		CurrentTimeOfDay = TheDay;
		TargetSunHeight = FMath::RandRange(0.5f, 1.0f);
		break;
	case ETimeOfDay::T_Evening:
		CurrentTimeOfDay = TheDay;
		TargetSunHeight = FMath::RandRange(-1.0f, 0.1f);
		break;
	default:
		break;
	}

	bUpdateSunPosition = true;
	DayNightCycleSpeed = Speed;
}

bool ATOMSkySphere::CheckAndCompareTasks(ECharacterTask CharacterTask)
{

	switch (CharacterTask)
	{
	case ECharacterTask::TA_None:
		break;
	case ECharacterTask::TA_DayONSoundOn:
	case ECharacterTask::TA_AfternoonOnSoundOn:
		if (GetTimeOfDay() == ETimeOfDay::T_Morning || GetTimeOfDay() == ETimeOfDay::T_Afternoon)
		{
			if (GameInstance)
			{
				if (!GameInstance->GetIsMusicPlaying())
				{
					return true;
				}
			}
		}
		break;
	case ECharacterTask::TA_DayONSoundOff:
	case ECharacterTask::TA_AfternoonOnSoundOff:

		if (GetTimeOfDay() == ETimeOfDay::T_Morning || GetTimeOfDay() == ETimeOfDay::T_Afternoon)
		{
			if (GameInstance)
			{
				if (GameInstance->GetIsMusicPlaying())
				{
					return true;
				}
			}
		}
		break;
	case ECharacterTask::TA_NightOnSoundOff:

		if (GetTimeOfDay() == ETimeOfDay::T_Evening)
		{
			if (GameInstance)
			{
				if (GameInstance->GetIsMusicPlaying())
				{
					return true;
				}
			}
		}
		break;
	case ECharacterTask::TA_NightOnSoundOn:

		if (GetTimeOfDay() == ETimeOfDay::T_Evening)
		{
			if (GameInstance)
			{
				if (!GameInstance->GetIsMusicPlaying())
				{
					return true;
				}
			}
		}
		break;
	default:
		break;
	}

	return false;
}

void ATOMSkySphere::StartWithRandomDay()
{

	//Random Time of day
	uint8 DayToStart = (uint8)(FMath::RandRange(1, 3));
	if (DayToStart == 1U)
	{
		SetTimeOfDay(ETimeOfDay::T_Morning, 10.0f);
	}
	else if (DayToStart == 2U)
	{
		SetTimeOfDay(ETimeOfDay::T_Afternoon, 10.0f);
	}
	else if (DayToStart == 3U)
	{
		SetTimeOfDay(ETimeOfDay::T_Evening, 10.0f);
	}
}

ETimeOfDay ATOMSkySphere::GetTimeOfDay()
{
	ETimeOfDay TheCurrentTimeOfDay = ETimeOfDay::T_None;

	switch (NextTimeOfDay)
	{
	case ETimeOfDay::T_None:
		TheCurrentTimeOfDay = ETimeOfDay::T_None;
		break;
	case ETimeOfDay::T_Morning:
		TheCurrentTimeOfDay = ETimeOfDay::T_Evening;
		break;
	case ETimeOfDay::T_Afternoon:
		TheCurrentTimeOfDay = ETimeOfDay::T_Morning;
		break;
	case ETimeOfDay::T_Evening:
		TheCurrentTimeOfDay = ETimeOfDay::T_Afternoon;
		break;
	default:
		break;
	}

	return TheCurrentTimeOfDay;
}

void ATOMSkySphere::UpdateTimeofDay()
{

	if (SunHeight >= 0.0f && SunHeight <= 0.5f)
	{
		CurrentTimeOfDay = ETimeOfDay::T_Morning;
		NextTimeOfDay = ETimeOfDay::T_Afternoon;
	}
	else if (SunHeight > 0.5f && SunHeight <= 1.0f)
	{
		CurrentTimeOfDay = ETimeOfDay::T_Afternoon;
		NextTimeOfDay = ETimeOfDay::T_Evening;
	}
	else if (SunHeight > -1.0f && SunHeight <= 0.0f)
	{
		CurrentTimeOfDay = ETimeOfDay::T_Evening;
		NextTimeOfDay = ETimeOfDay::T_Morning;
	}
}

void ATOMSkySphere::UpdateTimeofDayWidget()
{
	if (WidgetTimeDay == GetTimeOfDay())
	{
		return;
	}

	//Set to the current time of day
	WidgetTimeDay = GetTimeOfDay();

	if (GameInstance)
	{
		auto GameHUD = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
		if (GameHUD)
		{
			switch (WidgetTimeDay)
			{
			case ETimeOfDay::T_None:
				break;
			case ETimeOfDay::T_Morning:
				GameHUD->SetTimeDayImage(MorningImage);
				GameHUD->SetTimeDayText(ETimeOfDay::T_Morning);
				break;
			case ETimeOfDay::T_Afternoon:
				GameHUD->SetTimeDayImage(AfternoonImage);
				GameHUD->SetTimeDayText(ETimeOfDay::T_Afternoon);
				break;
			case ETimeOfDay::T_Evening:
				GameHUD->SetTimeDayImage(EveningImage);
				GameHUD->SetTimeDayText(ETimeOfDay::T_Evening);
				break;
			default:
				break;
			}
		}
	}
}

void ATOMSkySphere::UpdateDay(float DeltaTime, float DayNightSpeed)
{
	if (bUpdateSunPosition)
	{
		if (CurrentTimeOfDay == ETimeOfDay::T_Evening || CurrentTimeOfDay == ETimeOfDay::T_Afternoon)
		{
			if (TargetSunHeight < 0.0f)
			{
				if (SunHeight <= TargetSunHeight)
				{
					bUpdateSunPosition = false;
					TargetSunHeight = 0.0f;
					return;
				}
			}
		}

		if (TargetSunHeight > 0.0f)
		{
			if (SunHeight >= TargetSunHeight)
			{
				bUpdateSunPosition = false;
				TargetSunHeight = 0.0f;
				return;
			}
		}

		//Multiply by a factor to combat frame drops
		auto NewDeltaTime = DeltaTime * DayNightSpeed;

		//Make a new rotator
		auto SunRotator = UKismetMathLibrary::MakeRotator(0.0f, NewDeltaTime, 0.0f);

		if (DirectionLight)
		{
			//Update the Light Rotaion
			DirectionLight->AddActorLocalRotation(SunRotator);

			//Update the sun direction
			UpdateSunDirection();

			//Update the time of day
			UpdateTimeofDay();

			//Update the widget
			UpdateTimeofDayWidget();

			UE_LOG(LogTemp, Warning, TEXT("Sun Height: %s"), *FString::SanitizeFloat(SunHeight));

			UE_LOG(LogTemp, Warning, TEXT("Time Of Day: %d"), CurrentTimeOfDay);

			UE_LOG(LogTemp, Warning, TEXT("TargetSunHight: %d  , CurrentSunHeigt: %d "), TargetSunHeight, SunHeight);

		}
	}
}

