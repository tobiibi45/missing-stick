// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMWidgetBase.h"

/**Engine Includes*/
#include "ConstructorHelpers.h"

/**Game Includes*/
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"

UTOMWidgetBase::UTOMWidgetBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

bool UTOMWidgetBase::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	bIsFocusable = true;

	return true;

}

void UTOMWidgetBase::SetOwningActor(AActor* NewOwner)
{
	if (OwningActor == NewOwner)
	{
		// Skip repeated calls if owner is the same
		return;
	}

	//Updates the new owner
	OwningActor = NewOwner;

	//Broadcast the new owner change
	OnCreateInventoryItem.Broadcast(NewOwner);
}

class UTOMGameInstance* UTOMWidgetBase::GetMyGameInstance()
{
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());

	if (GameInstance)
	{
		return GameInstance;
	}

	return nullptr;
}

class ATOMPlayerController* UTOMWidgetBase::GetMyPlayersController()
{
	auto PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());

	if (PlayerController)
	{
		return PlayerController;
	}

	return nullptr;
}