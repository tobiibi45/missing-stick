// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMClockInter.h"

/**Engine Includes*/
#include "Components/StaticMeshComponent.h"

ATOMClockInter::ATOMClockInter()
{
	InteractableInfo.InteractableType = EItneractableType::I_Clock;
	InteractableInfo.InteractableClass = this;
	InteractableInfo.InteractableUClass = GetClass();
	InteractableInfo.InteractableName = FText::FromString(FString("DayTimeClock"));
	InteractableInfo.bIsPickable = false;
	InteractableInfo.SocketName = TEXT("None");
	InteractableInfo.LinkedUIName = TEXT("TimeOfDay");

	//Set the collision
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	//Set to simulate physics
	MeshComp->SetSimulatePhysics(false);
}
