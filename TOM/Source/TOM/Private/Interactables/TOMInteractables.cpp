// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMInteractables.h"

/**Game Includes*/
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMStickMap.h"
#include "TOMMainHUD.h"
#include "TOMCharacter.h"

/**Engine Includes*/
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "TimerManager.h"

// Sets default values
ATOMInteractables::ATOMInteractables()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Initializes the Scene comp
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));

	//Make the scene comp the root component
	RootComponent = SceneComp;

	// Initializes the mesh comp
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));

	//Set the collision
	MeshComp->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

	//Set to simulate physics
	MeshComp->SetSimulatePhysics(true);

	//Attach the mesh comp to the root component
	MeshComp->SetupAttachment(RootComponent);

	// Initializes the mesh comp
	OtherMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OtherMeshComp"));

	//Attach the mesh comp to the root component
	OtherMeshComp->SetupAttachment(RootComponent);

	// Initializes the Sphere comp
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));

	//Attach tot he root component
	SphereComp->SetupAttachment(MeshComp);

	//Allows the comp to generate overlap events
	SphereComp->SetGenerateOverlapEvents(true);

	//Block all channels for collision
	SphereComp->SetCollisionResponseToAllChannels(ECR_Block);

	//Only checks when a pawn overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);

	//Ignores collision when a camera overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Camera, ECollisionResponse::ECR_Ignore);

	//Ignores collision when visibility overlaps
	SphereComp->SetCollisionResponseToChannel(ECC_Visibility, ECollisionResponse::ECR_Ignore);

	//Show the sphere in game
	SphereComp->SetHiddenInGame(true);

	//This actor can't be damaged
	SetCanBeDamaged(false);

}

// Called when the game starts or when spawned
void ATOMInteractables::BeginPlay()
{
	Super::BeginPlay();
	
	//Bind the overlapping events to functions that act as the callback when event is triggered
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ATOMInteractables::OnInteractableBeginOverlap);
	SphereComp->OnComponentEndOverlap.AddDynamic(this, &ATOMInteractables::OnInteractableEndOverlap);

	auto MyWorld = GetWorld();
	if (ensure(MyWorld))
	{
		MyWorld->GetTimerManager().SetTimer(TimerHandle_ItemToSleep, this, &ATOMInteractables::SetItemToSleep, 5.0f, false);
	}
}

void ATOMInteractables::OnInteractableBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bIsOverlapping)
	{

		auto MyWorld = GetWorld();
		if (ensure(MyWorld))
		{
			//if the timer is active when a new interaction is going on, reset it
			if (MyWorld->GetTimerManager().IsTimerActive(TimerHandle_ItemToSleep))
			{
				MyWorld->GetTimerManager().ClearTimer(TimerHandle_ItemToSleep);
			}
		}

		if (InteractableInfo.InteractableType == EItneractableType::I_Item)
		{
			//Task Not Completed
			auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
			if (GameInstance)
			{
				auto MapWBP = Cast<UTOMStickMap>(GameInstance->FindActiveWidget(TEXT("Map")));
				if (MapWBP)
				{
					if (!MapWBP->IsMapCompleted())
					{
						return;
					}
					else
					{
						auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
						if (HudWBP)
						{
							FString TheString(TEXT("You have found the great BLACK stick!! Please go To the Grand Clock and present this great gift"));
							GameInstance->bStickFound = true;
							HudWBP->SetTaskInfoText(TheString);

							FString TheString1(TEXT("You found it!! Go to the gerat big clock"));
							HudWBP->SetObjectiveInfoText(TheString1);
						}
					}
				}
			}
		}

		//Wake rigid body
		MeshComp->WakeRigidBody();

		//Set the owner tot he actor overlapping
		SetInteractableOwner(OtherActor);

		if (InteractableInfo.InteractableType != EItneractableType::I_Clock)
		{
			//Send the interactable struct info to the character
			PlayerController->ReceiveInfo(InteractableInfo, this);
		}

		if (InteractableInfo.InteractableType != EItneractableType::I_Item)
		{
			//interact automatically
			OnInteract();

			auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
			if (GameInstance)
			{
				auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
				if (HudWBP)
				{
					HudWBP->SetStickImage();
				}
			}

		}

		if (InteractableInfo.InteractableType == EItneractableType::I_Clock)
		{
			if (PlayerController)
			{
				auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
				if (GameInstance->bStickFound)
				{
					if (GameInstance)
					{
						GameInstance->OnGameCompleted();
					}
				}
				else
				{
					if (GameInstance)
					{
						auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
						if (HudWBP)
						{
							FString String(TEXT("YOU NEED TO HELP THIS TOWN!! The Sacred Black Stick for me to function is MISSING!! Please find it. You can ask around for clues "));
							HudWBP->SetTaskInfoText(String);
						}
					}
				}
			}
		}

		//Set overlapping to true
		bIsOverlapping = true;

		UE_LOG(LogTemp, Warning, TEXT("Interacting with Object"))
	}
}

void ATOMInteractables::OnInteractableEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (bIsOverlapping)
	{
		//Remove the reference of the interactable item
		PlayerController->DeleteInfo(InteractableInfo, this);

		//Rest the owner
		RestetInteractableOwner();

		//Set overlapping to false
		bIsOverlapping = false;

		auto MyWorld = GetWorld();
		if (ensure(MyWorld))
		{
			MyWorld->GetTimerManager().SetTimer(TimerHandle_ItemToSleep, this, &ATOMInteractables::SetItemToSleep, 5.0f, false);
		}

		UE_LOG(LogTemp, Warning, TEXT("Stopped interacting"))
	}
}

void ATOMInteractables::SetItemToSleep()
{
	//put to sleep
	MeshComp->PutRigidBodyToSleep();
}

void ATOMInteractables::OnInteract()
{
	if (bIsOverlapping)
	{
		if (InteractableInfo.InteractableType == EItneractableType::I_Clock)
		{
			PlayerController->ShowUI(InteractableInfo.LinkedUIName);
			InteractableInfo.bUIIsOpened = true;

			if (!bFirstClockInteract)
			{
				auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
				if (GameInstance)
				{
					auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
					if (HudWBP)
					{
						FString String(TEXT("Find the 4 town members that are missing things and help them sort their issues "));
						HudWBP->SetObjectiveInfoText(String);

						bFirstClockInteract = true;
					}
				}
			}
		}

		PlayerController->PlayerInteract(InteractableInfo, this);
	}
}

void ATOMInteractables::SetInteractableOwner(AActor* OtherActor)
{
	//Set the owner of the item to the pawn that overlapped it
	SetOwner(OtherActor);

	//Set refs
	Character = Cast<ATOMCharacter>(OtherActor);
	PlayerController = Cast<ATOMPlayerController>(Character->GetController());

	if (Character)
	{
		Character->OnPlayerPickUp.AddDynamic(this, &ATOMInteractables::OnInteract);
	}
}

void ATOMInteractables::RestetInteractableOwner()
{
	//Remove the dynamic cast
	if (Character)
	{
		Character->OnPlayerPickUp.RemoveDynamic(this, &ATOMInteractables::OnInteract);
	}

	//Set refs
	Character = nullptr;
	PlayerController = nullptr;
}
