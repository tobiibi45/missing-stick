// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMStickInter.h"

ATOMStickInter::ATOMStickInter()
{
	InteractableInfo.InteractableType = EItneractableType::I_Item;
	InteractableInfo.InteractableClass = this;
	InteractableInfo.InteractableUClass = GetClass();
	InteractableInfo.InteractableName = FText::FromString(FString("Stick"));
	InteractableInfo.bIsPickable = true;
	InteractableInfo.SocketName = TEXT("None");
}
