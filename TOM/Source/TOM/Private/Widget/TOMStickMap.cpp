// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMStickMap.h"

/** Game Includes */
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMSkySphere.h"
#include "TOMMainHUD.h"

/**Engine Includes*/
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Engine/World.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

UTOMStickMap::UTOMStickMap(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

bool UTOMStickMap::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Hidden);

	//Allows the widget to be focusable
	bIsFocusable = true;

	//set defaults
	bTopLeft = false;
	bTopRight = false;
	bBottomLeft = false;
	bBottomRight = false;
	PercentageCounter = 0;
	PercentageCounterForMaps = 0;

	//Checks if the button is valid
	if (Button_Close)
	{
		//Binds the button on clicked event to the call back function
		Button_Close->OnClicked.AddDynamic(this, &UTOMStickMap::OnCloseClicked);
	}

	return true;
}

void UTOMStickMap::Setup()
{
	//Adds the widget to the players viewport
	this->AddToViewport();

	SetInputMode(false);

}

void UTOMStickMap::SetInputMode(bool bActive)
{
	if (this->IsInViewport())
	{
		if (bActive)
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeUIOnly GameUIOnly;

				GameUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
				GameUIOnly.SetWidgetToFocus(this->TakeWidget());

				PlayerController->SetInputMode(GameUIOnly);
				PlayerController->bShowMouseCursor = true;
			}
		}
		else
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeGameOnly GameOnly;

				PlayerController->SetInputMode(GameOnly);
				PlayerController->bShowMouseCursor = false;
			}
		}
	}
}

void UTOMStickMap::OnCloseClicked()
{
	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetIsOpened(false);
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Hidden);

	//Set back to game
	SetInputMode(false);

	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->HideUI(FString(TEXT("Map")));
	}
}

void UTOMStickMap::SetTopLeft()
{
	if (Image_TopLeft)
	{
		Image_TopLeft->SetBrushTintColor(FSlateColor(FLinearColor(1.0F, 1.0F, 1.0F, 0.0F)));
		bTopLeft = true;
		PercentageCounterForMaps++;
	}
}

void UTOMStickMap::SetTopRight()
{
	if (Image_TopRight)
	{
		Image_TopRight->SetBrushTintColor(FSlateColor(FLinearColor(1.0F, 1.0F, 1.0F, 0.0F)));
		bTopRight = true;
		PercentageCounterForMaps++;
	}
}

void UTOMStickMap::SetBottomLeft()
{
	if (Image_BottomLeft)
	{
		Image_BottomLeft->SetBrushTintColor(FSlateColor(FLinearColor(1.0F, 1.0F, 1.0F, 0.0F)));
		bBottomLeft = true;
	}
}

void UTOMStickMap::SetBottomRight()
{
	if (Image_BottomRight)
	{
		Image_BottomRight->SetBrushTintColor(FSlateColor(FLinearColor(1.0F, 1.0F, 1.0F, 0.0F)));
		bBottomRight = true;
		PercentageCounterForMaps++;
	}
}

void UTOMStickMap::SetPercentageText()
{
	if (TextBlock_Percentage)
	{
		PercentageCounter++;
		if (PercentageCounter == 1)
		{
			TextBlock_Percentage->SetText(FText::FromString(FString("25%")));
		}
		else if (PercentageCounter == 2)
		{
			TextBlock_Percentage->SetText(FText::FromString(FString("50%")));
		}
		else if (PercentageCounter == 3)
		{
			TextBlock_Percentage->SetText(FText::FromString(FString("75%")));
		}
		else if (PercentageCounter == 4)
		{
			TextBlock_Percentage->SetText(FText::FromString(FString("100%")));

			//Task Not Completed
			auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
			if (GameInstance)
			{
				auto HudWBP = Cast<UTOMMainHUD>(GameInstance->FindActiveWidget(TEXT("MainHUD")));
				if (HudWBP)
				{
					FString String(TEXT("Find the sacred stick. X marks the spot. Check your map "));
					HudWBP->SetObjectiveInfoText(String);
				}
			}
		}
	}
}

bool UTOMStickMap::IsMapCompleted()
{
	if (PercentageCounter >= 4)
	{
		return true;
	}

	return false;
}

int32 UTOMStickMap::GetPercantage()
{
	return PercentageCounterForMaps;
}

void UTOMStickMap::CleanUp()
{

	FInputModeGameOnly InputModeData;

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = false;
	}

	this->RemoveFromViewport();
	this->RemoveFromParent();

	UE_LOG(LogTemp, Warning, TEXT("HUD UnActive"));
}

void UTOMStickMap::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	CleanUp();
}
