// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMEndWidget.h"

/** Game Includes */
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMSkySphere.h"

/**Engine Includes*/
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "ConstructorHelpers.h"
#include "Components/AudioComponent.h"

UTOMEndWidget::UTOMEndWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundCue(TEXT("/Game/SoulCity/Sound/Cue/Music/Soul_Music_FlyUp01_Cue"));
	if (SoundCue.Succeeded())
	{
		//Set the parent material id succeeded
		EndMusic = SoundCue.Object;
	}
}

bool UTOMEndWidget::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Visible);

	//Allows the widget to be focusable
	bIsFocusable = true;

	//Checks if the button is valid
	if (Button_Exit)
	{
		//Binds the button on clicked event to the call back function
		Button_Exit->OnClicked.AddDynamic(this, &UTOMEndWidget::OnButtonClicked);
	}


	return true;
}

void UTOMEndWidget::Setup()
{
	//Adds the widget to the players viewport
	this->AddToViewport();

	SetInputMode(true);

	if (EndMusic)
	{
		auto World = GetWorld();
		if (World)
		{
			AudioComponent = UGameplayStatics::SpawnSound2D(this, EndMusic);
			if (AudioComponent)
			{
				AudioComponent->SetActive(true);
			}
		}
	}

}

void UTOMEndWidget::SetInputMode(bool bActive)
{
	if (this->IsInViewport())
	{
		if (bActive)
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeUIOnly UIOnly;

				UIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
				UIOnly.SetWidgetToFocus(this->TakeWidget());

				PlayerController->SetInputMode(UIOnly);
				PlayerController->bShowMouseCursor = true;
			}
		}
		else
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeGameOnly GameOnly;

				PlayerController->SetInputMode(GameOnly);
				PlayerController->bShowMouseCursor = false;
			}
		}
	}
}

void UTOMEndWidget::CleanUp()
{
	if (AudioComponent)
	{
		AudioComponent->SetActive(false);
	}

	FInputModeGameOnly InputModeData;

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = false;
	}

	this->RemoveFromViewport();
	this->RemoveFromParent();

	UE_LOG(LogTemp, Warning, TEXT("HUD UnActive"));
}

void UTOMEndWidget::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	CleanUp();
}

void UTOMEndWidget::OnButtonClicked()
{
	CleanUp();

	TEnumAsByte<EQuitPreference::Type> Byte = EQuitPreference::Quit;

	UWorld* World = GetWorld();
	if (!World)
	{
		return;
	}

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		UKismetSystemLibrary::QuitGame(World, PlayerController, Byte, false);
	}
}
