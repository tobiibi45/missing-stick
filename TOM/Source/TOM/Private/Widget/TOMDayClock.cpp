// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMDayClock.h"

/** Game Includes */
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMSkySphere.h"

/**Engine Includes*/
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Engine/World.h"

UTOMDayClock::UTOMDayClock(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

bool UTOMDayClock::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Hidden);

	//Allows the widget to be focusable
	bIsFocusable = true;

	//Checks if the button is valid
	if (Button_Morning)
	{
		//Binds the button on clicked event to the call back function
		Button_Morning->OnClicked.AddDynamic(this, &UTOMDayClock::OnMorningClicked);
	}

	//Checks if the button is valid
	if (Button_Afternoon)
	{
		//Binds the button on clicked event to the call back function
		Button_Afternoon->OnClicked.AddDynamic(this, &UTOMDayClock::OnAfternoonClicked);
	}

	//Checks if the button is valid
	if (Button_Evening)
	{
		//Binds the button on clicked event to the call back function
		Button_Evening->OnClicked.AddDynamic(this, &UTOMDayClock::OnEveningClicked);
	}

	//Checks if the button is valid
	if (Button_Close)
	{
		//Binds the button on clicked event to the call back function
		Button_Close->OnClicked.AddDynamic(this, &UTOMDayClock::OnCloseClicked);
	}

	//Checks if the button is valid
	if (Button_SoundOn)
	{
		//Binds the button on clicked event to the call back function
		Button_SoundOn->OnClicked.AddDynamic(this, &UTOMDayClock::OnSoundOnButtonClicked);
	}

	//Checks if the button is valid
	if (Button_SoundOff)
	{
		//Binds the button on clicked event to the call back function
		Button_SoundOff->OnClicked.AddDynamic(this, &UTOMDayClock::OnSoundOffButtonClicked);
	}

	return true;
}

void UTOMDayClock::Setup()
{
	//Adds the widget to the players viewport
	this->AddToViewport();

	SetInputMode(false);

}

void UTOMDayClock::SetInputMode(bool bActive)
{
	if (this->IsInViewport())
	{
		if (bActive)
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeUIOnly GameUIOnly;

				GameUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
				GameUIOnly.SetWidgetToFocus(this->TakeWidget());

				PlayerController->SetInputMode(GameUIOnly);
				PlayerController->bShowMouseCursor = true;
			}
		}
		else
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeGameOnly GameOnly;

				PlayerController->SetInputMode(GameOnly);
				PlayerController->bShowMouseCursor = false;
			}
		}
	}
}

void UTOMDayClock::CleanUp()
{

	FInputModeGameOnly InputModeData;

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = false;
	}

	this->RemoveFromViewport();
	this->RemoveFromParent();

	UE_LOG(LogTemp, Warning, TEXT("HUD UnActive"));
}

void UTOMDayClock::OnMorningClicked()
{
	for (TActorIterator<ATOMSkySphere> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
		TheSkySphere = *ActorItr;
	}

	if (TheSkySphere)
	{
		TheSkySphere->SetTimeOfDay(ETimeOfDay::T_Morning);
	}
}

void UTOMDayClock::OnAfternoonClicked()
{
	for (TActorIterator<ATOMSkySphere> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
		TheSkySphere = *ActorItr;
	}

	if (TheSkySphere)
	{
		TheSkySphere->SetTimeOfDay(ETimeOfDay::T_Afternoon);
	}
}

void UTOMDayClock::OnEveningClicked()
{
	for (TActorIterator<ATOMSkySphere> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
		TheSkySphere = *ActorItr;
	}

	if (TheSkySphere)
	{
		TheSkySphere->SetTimeOfDay(ETimeOfDay::T_Evening);
	}
}

void UTOMDayClock::OnCloseClicked()
{
	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetIsOpened(false);
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Hidden);

	//Set to game
	SetInputMode(false);

	//Hide UI
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->HideUI(FString(TEXT("TimeOfDay")));
	}
}

void UTOMDayClock::OnSoundOnButtonClicked()
{
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->SetMusicPlaying(false);
	}
}

void UTOMDayClock::OnSoundOffButtonClicked()
{
	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->SetMusicPlaying(true);
	}
}

void UTOMDayClock::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	CleanUp();
}
