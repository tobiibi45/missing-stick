// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMBeginWidget.h"

/** Game Includes */
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMSkySphere.h"

/**Engine Includes*/
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Engine/World.h"
#include "ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

UTOMBeginWidget::UTOMBeginWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Finds and loads the Horizon Linear Curve
	static ConstructorHelpers::FObjectFinder<USoundCue> SoundCue(TEXT("/Game/SoulCity/Sound/Cue/Music/Soul_Music_FlyUp01_Cue"));
	if (SoundCue.Succeeded())
	{
		//Set the parent material id succeeded
		MenuMusic = SoundCue.Object;
	}
}

bool UTOMBeginWidget::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Visible);

	//Allows the widget to be focusable
	bIsFocusable = true;

	//Checks if the button is valid
	if (Button_Start)
	{
		//Binds the button on clicked event to the call back function
		Button_Start->OnClicked.AddDynamic(this, &UTOMBeginWidget::OnButtonClicked);
	}

	return true;
}

void UTOMBeginWidget::Setup()
{
	//Adds the widget to the players viewport
	this->AddToViewport();

	SetInputMode(true);

	if (MenuMusic)
	{
		auto World = GetWorld();
		if (World)
		{
			AudioComponent = UGameplayStatics::SpawnSound2D(this, MenuMusic);
			if (AudioComponent)
			{
				AudioComponent->SetActive(true);
			}
		}
	}

}

void UTOMBeginWidget::SetInputMode(bool bActive)
{
	if (this->IsInViewport())
	{
		if (bActive)
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeUIOnly UIOnly;

				UIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
				UIOnly.SetWidgetToFocus(this->TakeWidget());

				PlayerController->SetInputMode(UIOnly);
				PlayerController->bShowMouseCursor = true;
			}
		}
		else
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeGameOnly GameOnly;

				PlayerController->SetInputMode(GameOnly);
				PlayerController->bShowMouseCursor = false;
			}
		}
	}
}

void UTOMBeginWidget::CleanUp()
{
	if (AudioComponent)
	{
		AudioComponent->SetActive(false);
	}

	FInputModeGameOnly InputModeData;

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = false;
	}

	this->RemoveFromViewport();
	this->RemoveFromParent();

	UE_LOG(LogTemp, Warning, TEXT("HUD UnActive"));
}

void UTOMBeginWidget::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	CleanUp();
}

void UTOMBeginWidget::OnButtonClicked()
{
	CleanUp();

	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->OnGameBegins();
	}
}

