// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

/**Header Includes*/
#include "TOMMainHUD.h"

/** Game Includes */
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMDayClock.h"
#include "TOMStickMap.h"

/** Engine Includes */
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/Texture2D.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "TimerManager.h"

UTOMMainHUD::UTOMMainHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

bool UTOMMainHUD::Initialize()
{
	bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	//Set the HUD to be visible to allow drag drop events
	SetVisibility(ESlateVisibility::Visible);

	//Allows the widget to be focusable
	bIsFocusable = true;

	return true;
}

void UTOMMainHUD::Setup()
{
	//Adds the widget to the players viewport
	this->AddToViewport();

	FString String(TEXT("Thanks for coming! Please look around and try solve the mystery of the missing Stick. Beware tho, People here are always missing something. You may need to fullfill their wishes"));
	SetTaskInfoText(String, 20.0F);
}

void UTOMMainHUD::SetInputMode(bool bActive)
{
	if (this->IsInViewport())
	{
		if (bActive)
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeUIOnly GameUI;

				GameUI.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
				GameUI.SetWidgetToFocus(this->TakeWidget());

				PlayerController->SetInputMode(GameUI);
				PlayerController->bShowMouseCursor = true;
			}
		}
		else
		{
			//Gets the player controller
			ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
			if (ensure(PlayerController))
			{
				FInputModeGameOnly GameOnly;

				PlayerController->SetInputMode(GameOnly);
				PlayerController->bShowMouseCursor = false;
			}
		}
	}
}

void UTOMMainHUD::SetTimeDayImage(class UTexture2D* Image)
{
	//Checks if the button is valid
	if (Image_Icon)
	{
		SlateBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(Image);
		Image_Icon->SetBrush(SlateBrush);
		Image_Icon->SetBrushSize(FVector2D(50.0f, 50.0f));
	}
}

void UTOMMainHUD::SetTimeDayText(ETimeOfDay TimeOfDay)
{
	ETimeOfDay CurrentTimeOfDay = TimeOfDay;

	//Checks if the button is valid
	if (Text_TimeOfDay)
	{
		switch (CurrentTimeOfDay)
		{
		case ETimeOfDay::T_None:
			return;
			break;
		case ETimeOfDay::T_Morning:
			Text_TimeOfDay->SetText(FText::FromString(FString("Morning")));
			break;
		case ETimeOfDay::T_Afternoon:
			Text_TimeOfDay->SetText(FText::FromString(FString("Afternoon")));
			break;
		case ETimeOfDay::T_Evening:
			Text_TimeOfDay->SetText(FText::FromString(FString("Evening")));
			break;
		default:
			break;
		}
	}
}

void UTOMMainHUD::SetTaskInfoText(FString& Infostr, float length /*= 5.0f*/)
{
	//Checks if the button is valid
	if (TextBlock_TasksInfo)
	{
		TextBlock_TasksInfo->SetText(FText::FromString(Infostr));

		auto MyWorld = GetWorld();
		if (ensure(MyWorld))
		{
			MyWorld->GetTimerManager().SetTimer(TimerHandle_CkearTaskInfo, this, &UTOMMainHUD::OnClearTaskInfoText, length, false);
		}
	}
}

void UTOMMainHUD::SetObjectiveInfoText(FString& Infostr)
{
	//Checks if the text is valid
	if (TextBlock_Objectives)
	{
		TextBlock_Objectives->SetText(FText::FromString(Infostr));
	}
}

void UTOMMainHUD::SetStickImage()
{
	//Checks if the image is valid
	if (Image_Stick)
	{
		Image_Icon->SetColorAndOpacity(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

void UTOMMainHUD::OnClearTaskInfoText()
{
	TextBlock_TasksInfo->SetText(FText::FromString(TEXT("")));

	if (!bFirstInteract)
	{
		//First objective
		FString String(TEXT("Find the great big clock and interact with it to begin your journey"));
		SetObjectiveInfoText(String);

		bFirstInteract = true;
	}
}

class UTOMStickMap* UTOMMainHUD::GetMapRef()
{
	if (WBP_StickMap)
	{
		return WBP_StickMap;
	}

	return nullptr;
}

class UTOMDayClock* UTOMMainHUD::GetDayTimeRef()
{
	if (WBP_TimeDayClock)
	{
		return WBP_TimeDayClock;
	}

	return nullptr;
}

void UTOMMainHUD::CleanUp()
{

	FInputModeGameOnly InputModeData;

	ATOMPlayerController* PlayerController = Cast<ATOMPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputModeData);
		PlayerController->bShowMouseCursor = false;
	}

	this->RemoveFromViewport();

	UE_LOG(LogTemp, Warning, TEXT("HUD UnActive"));
}

void UTOMMainHUD::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
}