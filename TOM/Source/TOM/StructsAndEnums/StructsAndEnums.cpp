// Copyright 2019 Krankyz Stuidos, All Rights Reserved.


/**Header Includes*/
#include "StructsAndEnums.h"

bool operator==(const FInteractables& lhs, const FInteractables& rhs)
{
	return lhs.InteractableClass == rhs.InteractableClass &&
		lhs.InteractableUClass == rhs.InteractableUClass;
}


bool operator!=(const FInteractables& lhs, const FInteractables& rhs)
{
	return lhs.InteractableClass != rhs.InteractableClass &&
		lhs.InteractableUClass != rhs.InteractableUClass;
}

bool operator==(const FTasks& lhs, const FTasks& rhs)
{
	return lhs.InteractableClass == rhs.InteractableClass &&
		lhs.InteractableUClass == rhs.InteractableUClass;
}


bool operator!=(const FTasks& lhs, const FTasks& rhs)
{
	return lhs.InteractableClass != rhs.InteractableClass &&
		lhs.InteractableUClass != rhs.InteractableUClass;
}

