// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/** Engine Includes */
#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Object.h"

/** Auto Generated Includes */
#include "StructsAndEnums.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ETimeOfDay : uint8
{
	T_None							UMETA(DisplayName = "None"),
	T_Morning						UMETA(DisplayName = "Morning"),
	T_Afternoon						UMETA(DisplayName = "Afternoon"),
	T_Evening						UMETA(DisplayName = "Evening")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EItneractableType : uint8
{
	I_None 							UMETA(DisplayName = "None"),
	I_Weapon						UMETA(DisplayName = "Weapon"),
	I_Item							UMETA(DisplayName = "Item"),
	I_Clock							UMETA(DisplayName = "Clock")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ECharacterTask : uint8
{
	TA_None 						UMETA(DisplayName = "None"),
	TA_DayONSoundOn					UMETA(DisplayName = "DayONSoundOn"),
	TA_NightOnSoundOff				UMETA(DisplayName = "NightOnSoundOff"),
	TA_AfternoonOnSoundOn			UMETA(DisplayName = "AfternoonOnSoundOn"),
	TA_DayONSoundOff				UMETA(DisplayName = "DayONSoundOff"),
	TA_NightOnSoundOn				UMETA(DisplayName = "NightOnSoundOn"),
	TA_AfternoonOnSoundOff			UMETA(DisplayName = "AfternoonOnSoundOff")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EMapPart : uint8
{
	M_None 						UMETA(DisplayName = "None"),
	M_TopLeft					UMETA(DisplayName = "TopLeft"),
	M_TopRight					UMETA(DisplayName = "TopRight"),
	M_BottomRight				UMETA(DisplayName = "BottomRight"),
	M_BottomLeft				UMETA(DisplayName = "BottomLeft")
};

USTRUCT(BlueprintType)
struct FTasks //Always add generated_body when making new struct or classes
{
	GENERATED_USTRUCT_BODY()

	/* The UClass of the intractable. This is essential to spawn in the world
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	UClass* InteractableUClass;


	/** The class of the intractable
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	class ATOMInteractables* InteractableClass;


	/** The name of the socket it gets attached to
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	FName TaskName;


	/** Name of the Linked UI associated with the object if applicable
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	FString CompletedTaskText;


	/** Name of the Linked UI associated with the object if applicable
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	FString UnCompletedTaskText;


	/** If the Linked UI is opened
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	bool bIsCompleted;


	/** The Character task that needs to be achieved
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	ECharacterTask CharacterTasks;


	/** The Character task that needs to be achieved
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tasks")
	EMapPart MapPart;


	FTasks() : InteractableUClass(nullptr), InteractableClass(nullptr), TaskName(TEXT("None")), CompletedTaskText(TEXT("")), UnCompletedTaskText(TEXT(""))
		, bIsCompleted(false), CharacterTasks(ECharacterTask::TA_None), MapPart(EMapPart::M_None)
	{
	}
};

bool operator == (const FTasks& lhs, const FTasks& rhs);
bool operator != (const FTasks& lhs, const FTasks& rhs);

USTRUCT(BlueprintType)
struct FInteractables//Always add generated_body when making new struct or classes
{
	GENERATED_USTRUCT_BODY()

		/* The UClass of the intractable. This is essential to spawn in the world
		*   UPROPERTY  Exposes the variable to blueprint
		*   EditAnywhere  Can be edited from anywhere in blueprint
		*BlueprintReadWrite  Allows blueprint to read/write the data it holds
		*/
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		UClass* InteractableUClass;


	/** The class of the intractable
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		class ATOMInteractables* InteractableClass;


	/** The intractable actors texture icon. this is the icon shown in the inventory. Helps to know what is what by looking
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		class UTexture2D* InteractableIcon;


	/** The name of the intractable. Example Big Black Sword
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		FText InteractableName;


	/** The name of the socket it gets attached to
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		FName SocketName;


	/** The index keeps track of where the intractable actor is in a character inventory
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		int32 Index;


	/** Boolean that sets if the item has been picked by a character
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		bool bHasBeenPicked;


	/** Boolean that sets if the item is in the inventory or action bar
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		bool bIsInInventory;


	/** If the inventory type can be carried on the character or not
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		bool bIsPickable;

	/** The Interactable type. Example, Weapon, Items and so on.
*   UPROPERTY  Exposes the variable to blueprint
*   EditAnywhere  Can be edited from anywhere in blueprint
*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
		EItneractableType InteractableType;


	/** Name of the Linked UI associated with the object if applicable
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
	FString LinkedUIName;


	/** If the Linked UI is opened
	*   UPROPERTY  Exposes the variable to blueprint
	*   EditAnywhere  Can be edited from anywhere in blueprint
	*	BlueprintReadWrite  Allows blueprint to read/write the data it holds
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactables")
	bool bUIIsOpened;


	FInteractables() : InteractableUClass(nullptr), InteractableClass(nullptr), InteractableIcon(nullptr), InteractableName(), Index(0), bHasBeenPicked(false),
		bIsInInventory(false), bIsPickable(false), InteractableType(EItneractableType::I_None), LinkedUIName(""), bUIIsOpened(false)
	{
	}
};

bool operator == (const FInteractables& lhs, const FInteractables& rhs);
bool operator != (const FInteractables& lhs, const FInteractables& rhs);