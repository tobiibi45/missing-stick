// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

/**Header Includes*/
#include "TOMGameMode.h"

/**Game Includes*/
#include "TOMCharacter.h"
#include "TOMPlayerController.h"
#include "TOMGameInstance.h"
#include "TOMMainHUD.h"
#include "TOMBeginWidget.h"

/**Engine Includes*/
#include "UObject/ConstructorHelpers.h"

ATOMGameMode::ATOMGameMode()
	: TheGameBeginWidget(nullptr)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//Set the default player controller class
	if (ATOMPlayerController::StaticClass())
	{
		PlayerControllerClass = ATOMPlayerController::StaticClass();
	}
}

void ATOMGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);

}

APlayerController* ATOMGameMode::Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString& Portal, const FString& Options, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	return Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);
}

void ATOMGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		TheGameBeginWidget = GameInstance->FindWidgetSubClass(TEXT("GameBegin"));
		if (TheGameBeginWidget)
		{
			GameInstance->CreateNewWidget(TheGameBeginWidget, TEXT("GameBegin"));
		}
	}
}


void ATOMGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	auto GameInstance = Cast<UTOMGameInstance>(GetGameInstance());
}