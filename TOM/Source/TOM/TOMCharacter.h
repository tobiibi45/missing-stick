// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

/** Engine Includes */
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

/** Auto Generated Includes */
#include "TOMCharacter.generated.h"

//Custom Event for when player picks up an item
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPickUp);


UCLASS(config=Game, meta = (BlueprintSpawnableComponent))
class ATOMCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	/** Sets default values for this character's properties */
	ATOMCharacter();

	/** Custom event triggered for when player picks up an item */
	UPROPERTY(BlueprintAssignable)
	FOnPickUp OnPlayerPickUp;

protected:

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArmComp;


	/** Actual Camera that shows us what we see*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComp;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Camera")
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Camera")
	float BaseLookUpRate;

	/** boolean that tells when a WBP window is opened*/
	bool bIsOpened;

protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;


	/** Responsible for moving the player forward and backwards
	**	Uses the Forward vector of the character
	**  @param Value  The value is either 1 or -1, depending on players input
	*/
	void MoveForward(float Value);


	/** Responsible for moving the player right and left
	**	Uses the Right vector of the character
	**	@param Value  The value is either 1 or -1, depending on players input
	*/
	void MoveRight(float Value);


	/** Responsible for the left, right rotation
	**	@param Rate
	*/
	void TurnAtRate(float Rate);


	/** Responsible for the up, down rotation
	**	@param Rate
	*/
	void LookUpAtRate(float Rate);


	/** Responsible for interacting with things
	*/
	void Interact();


	/** Responsible for opening the treasure map
	*/
	void OpenTreasureMap();

protected:

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return SpringArmComp; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return CameraComp; }
};

