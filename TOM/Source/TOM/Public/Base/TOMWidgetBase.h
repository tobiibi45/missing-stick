// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/** Engine Includes */
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

/** Auto Generated Includes */
#include "TOMWidgetBase.generated.h"

//Custom event for when the widget owner is changed
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOwningActorChanged, AActor*, NewOwner);

/**
 * 
 */
UCLASS()
class TOM_API UTOMWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:

	// Sets default values for this component's properties
	UTOMWidgetBase(const FObjectInitializer& ObjectInitializer);


	/* Custom event */
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "DW|ActorWidget")
	FOnOwningActorChanged OnCreateInventoryItem;

protected:

	/* Actor that widget is attached to via WidgetComponent */
	UPROPERTY(BlueprintReadOnly, Category = "DW|ActorWidget")
	AActor* OwningActor = nullptr;


	/* Name of the widget */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DW|ActorWidget")
	FString WidgetName = "Default";


	//Pointer to player controller object
	UPROPERTY()
	class ATOMPlayerController* OwnerPlayerController = nullptr;


	/** Audio Comp ref*/
	UPROPERTY()
	UAudioComponent* AudioComponent = nullptr;

public:

	/* Set the owning actor so widgets have access to whatever is, broadcasting OnOwningActorChanged event */
	UFUNCTION(BlueprintCallable, Category = "DW|ActorWidget")
	virtual void Init() {};


	/** Function to allow Widget class to set up what it needs */
	virtual bool Initialize() override;


	/* Set the owning actor so widgets have access to whatever is, broadcasting OnOwningActorChanged event */
	UFUNCTION(BlueprintCallable, Category = "DW|ActorWidget")
	void SetOwningActor(AActor* NewOwner);


	/** Function that adds the widget to the viewport, enabling the players to see it */
	virtual void Setup() {};


	/** Function that sets the input mode for a widget */
	virtual void SetInputMode(bool bActive) {};


	/** Function that allows the widget class to do any clean up needed */
	virtual void CleanUp() {};

	/** Function that return game instance pointer */
	virtual class UTOMGameInstance* GetMyGameInstance();

	/** Function that returns the owner's player controller  */
	virtual class ATOMPlayerController* GetMyPlayersController();
	
};
