// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/** Game Includes */
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

/** Auto Generated Includes */
#include "TOMGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API UTOMGameInstance : public UGameInstance
{
	GENERATED_BODY()

	//Type def for temp array tuple
	typedef TTuple< const FString, TSubclassOf<class UUserWidget>> WIDGETS;

public:

	/** Constructor
	*	@param ObjectInitializer   Used to get object and initialize them if needed
	*/
	UTOMGameInstance(const FObjectInitializer& ObjectInitializer);

protected:

	/** Map containing all the subclasses of widget needed */
	TMap< const FString, TSubclassOf<class UUserWidget> > WidgetArray;


	/** Map containing all active widget on viewport */
	TMap< const FString, class UTOMWidgetBase* > ActiveWidgets;


	/** Sub class for a user widget */
	TSubclassOf<class UUserWidget> MainHudSubClass;

	/** Sound cue ref*/
	class USoundCue* NormalMusic;

	/** Audio Comp ref*/
	UPROPERTY()
	class UAudioComponent* AudioComponent = nullptr;


	/** Main Hud ref*/
	UPROPERTY()
	class UTOMMainHUD* MainHudRef = nullptr;

public:

	/**boolean that tracks if HUD is active or not*/
	bool bMainHudOpened;


	/** Boolean hats tracks when the stick is found */
	bool bStickFound = false;


	/** Boolean hats tracks when the stick is found */
	bool bGameHasStarted = false;

public:


	/** Function to allow GameInstance to set up what it needs */
	virtual void Init() override;


	/** Function to allow GameInstance to do cleanup when shutting down */
	virtual void Shutdown() override;


	/** Function that loads, opens and closes the inventory ui
	*/
	class UTOMWidgetBase* CreateNewWidget(TSubclassOf<class UUserWidget>& Widget, const FString& WidgetName);


	/** Function that loads, opens and closes the Main HUD
	*/
	void RemoveWidget(const FString& Name);

	/** Function that loads, opens and closes the Main HUD
	*/	
	void OnGameCompleted();

	/** Function that loads, opens and closes the Main HUD
	*/
	void OnGameBegins();

	/** Function that gets if music is playing
	*/
	bool GetIsMusicPlaying();

	/** Function that sets to play music or not
	*/
	void SetMusicPlaying(bool bActive);

	/** Function that shows the map
	*/
	void ShowUI(const FString& Name);

	/** Function that hides the map
	*/
	void HideUI(const FString& Name);

	/** Function that finds and returns widgets in map
	*/
	TSubclassOf<class UUserWidget>& FindWidgetSubClass(const FString& Name);


	/** Function that finds and returns widgets in map
	*/
	class UTOMWidgetBase* FindActiveWidget(const FString& Name);

	
};
