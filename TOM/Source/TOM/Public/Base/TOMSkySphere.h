// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/** Game Includes */
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

/** Auto Generated Includes */
#include "TOMSkySphere.generated.h"

UCLASS()
class TOM_API ATOMSkySphere : public AActor
{
	GENERATED_BODY()
	
public:	

	// Sets default values for this actor's properties
	ATOMSkySphere();


	/** The scene component. The base root */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComp;


	/** The sky sphere*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComp;


	/** The Directional light source, Basically the sun */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class ADirectionalLight* DirectionLight;


	/** Horizon Linear Color Curve*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UCurveLinearColor* HorizonCurveColor;


	/** Zenith Linear Color Curve*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UCurveLinearColor* ZenithCurveColor;


	/** Cloud Linear Color Curve*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	 class UCurveLinearColor* CloudCurveColor;


	/** The Parent Material for the sky */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UMaterial* ParentMaterial;


	/** The Dynamic material instance which gets used by the sky */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UMaterialInstanceDynamic* MaterialInst;


	/** The Morning image texture*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UTexture2D* MorningImage;


	/** The Afternoon image texture*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UTexture2D* AfternoonImage;


	/** The Evening image texture*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TOM|Sky", meta = (AllowPrivateAccess = "true"))
	class UTexture2D* EveningImage;


	/** The Game instance ref*/
	UPROPERTY()
	class UTOMGameInstance* GameInstance;

protected:


	/** Boolean Values */

	/** Boolean for refreshing the sky material*/
	bool bRefreshMaterial;

	/** Boolean for determining the sun position*/
	bool bDetermineSunPosition;

	/** Boolean for determining if we update the sun*/
	bool bUpdateSunPosition;


	/** Vector param Values */

	/** Light direction name */
	FName LightDirectionName;

	/** Sun color name*/
	FName SunColorName;

	/** Horizon color name*/
	FName HorizonColorName;

	/** Zenith color name*/
	FName ZenithColorName;

	/** Colour color name*/
	FName CloudColorName;

	/** Horizon Fall off color name*/
	FName HorizonFallOffName;

	/** Overall color name*/
	FName OverallColorName;


	/** Scalar param Values */

	/** Cloud Speed name*/
	FName CloudSpeedName;

	/** Sun Brightness name*/
	FName SunBrightnessName;

	/** Sun height name*/
	FName SunHeightName;

	/** Cloud Opacity name*/
	FName CloudOpacityName;

	/** Stars brightness name*/
	FName StarsBrightnessName;


	/**Float Values */

	/** Sun Height*/
	float SunHeight;

	/** Horizon fall off*/
	float HorizonFallOff;

	/** Sun Brightness*/
	float SunBrightness;

	/** Cloud Speed*/
	float CloudSpeed;

	/** Cloud Opacity*/
	float CloudOpacity;

	/** Stars Brightness*/
	float StarsBrightness;


	/**Linear Colour */

	/** Zenith Linear Color*/
	FLinearColor ZenithColor;

	/** Horizon Linear Color*/
	FLinearColor HorizonColor;

	/** Cloud Linear Color*/
	FLinearColor CloudColor;

	/** Overall Linear Color*/
	FLinearColor OverallColor;


	/**Other Values */

	/** How fast the day night cycle lasts*/
	float DayNightCycleSpeed;

	/** How fast the day night cycle lasts*/
	float TargetSunHeight;


	/**Structs and Enums */

	/** Current Time of Day*/
	ETimeOfDay CurrentTimeOfDay;

	/** Next Time of Day*/
	ETimeOfDay NextTimeOfDay;

	/** Current Time of Day shown in the widget*/
	ETimeOfDay WidgetTimeDay;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	/** Function that refreshes the material for the sky 
	*/
	void RefreshMaterial();


	/** Function that refreshes the material for the sky
	*	@param LinearCurve - The linear color curve to be used
	*	@param InVlaue - The value used to get the color from the curve
	*/
	FLinearColor GetClampedLinearColor(class UCurveLinearColor* LinearCurve, float InValue);


	/** Function that calculates the light direction
	*	@param Light - The Directional light object
	*/
	void CalculateLightDiretion(class ADirectionalLight* Light);


	/** Function that calculates the sun height
	*	@param Light - The Directional light object
	*/
	void CalculateSunHeight(class ADirectionalLight* Light);


	/** Function that refreshes the material for the sky
	*	@param bDetermineByColors - Should we use sun position to determine colors
	*/
	void DetermineColorsBySunPosition(bool bDetermineColors);


	/** Function that updates the direction of the sun
	*/
	void UpdateSunDirection();


	/** Function that updates the direction of the sun
	*	@param DeltaTime - The frame time per seconds. Mostly deltat time from tick
	*	@param DayNightSpeed - The speed of the sun
	*/
	void UpdateDay(float DeltaTime, float DayNightSpeed);


	/** Function that updates the direction of the sun
	*/
	void UpdateTimeofDay();


	/** Function that updates the widget when the time of day changes
	*/
	void UpdateTimeofDayWidget();

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	/** Function that updates the direction of the sun
	*	@param bUpdateDay - True and the day updates or vice versa
	*	@param Speed - The speed of the sun/moon
	*/
	void SetUpdateDayCycle(bool bUpdateDay, float Speed);


	/** Function that updates the direction of the sun
	*	@param TheDay - What time of day should it be
	*	@param Speed - The speed of the sun/moon
	*/
	void SetTimeOfDay(ETimeOfDay TheDay, float Speed = 30.0f);


	/** Function that check compare and check tasks
	*	@param CharacterTask - Compare Tasks with time of day
	*/
	bool CheckAndCompareTasks(ECharacterTask CharacterTask);


	/** Function that check compare and check tasks */
	void StartWithRandomDay();


	/** Function that updates the direction of the sun
	*/
	ETimeOfDay GetTimeOfDay();
};
