// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

/** Auto Generated Includes */
#include "TOMPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API ATOMPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	//Type def for temp array tuple
	typedef TTuple< EItneractableType, class ATOMInteractables* > FTEMPITEMS;

public:

	/** Sets default values for this actor's properties */
	ATOMPlayerController();

protected:

	/** Pointer object to the character class */
	UPROPERTY()
	class ATOMCharacter* pCharacter = nullptr;


	/** Pointer object to the Game instance class */
	UPROPERTY()
	class UTOMGameInstance* pGameInstance = nullptr;


	/**Array of all objects that were overlapped*/
	TArray<FTEMPITEMS> FOverlappedItems;


	/**Array of all objects in the Players inventory*/
	TArray<FInteractables> FInventoryObjects;


	/** Sub class for a user widget */
	TSubclassOf<class UUserWidget> FoundUI;


	/** If a widget is opened*/
	bool bIsOpened = false;

	/** Pointer to the Main HUD */
	TSubclassOf<class UUserWidget> MapUI;

public:

	/** Functions that finds the stick
	*/
	bool IsStickFound() const;

	/** Functions that adds the interacted item to the players inventory
	*   @param InteractableInfo - Struct containing the intractable item info
	*/
	void PlayerInteract(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr);


	/** Functions that open a UI
	*   @param UiName - Name of the UI to Open
	*/
	void ShowUI(const FString& UiName);


	/** Functions that closes a UI
	*   @param UiName - Name of the UI to Open
	*/
	void CloseUI(const FString& UiName);


	/** Functions that receives and adds the item information when overlapping with an item to a temp array
	*   @param InteractableInfo - Struct containing the intractable item info
	*/
	void ReceiveInfo(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr);


	/** Functions that sets the status of the ui
	*   @param bIsOpen
	*/
	void SetIsOpened(bool bIsOpen);


	/** Functions that removes the item information data from the temp array when player stopped overlapping with the item
	*   @param InteractableInfo - Struct containing the intractable item info
	*/
	void DeleteInfo(const FInteractables& InteractableInfo, class ATOMInteractables* Interactableptr);

protected:

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;


	/** Functions that returns an empty slot in the array
	*   @param StorageType - What storage type are we modifying
	*/
	int32 FindEmptySlot() const;


	/** Functions that removes the item information data from the temp array when player stopped overlapping with the item
*   @param InteractableInfo - Struct containing the intractable item info
*   @param StorageType - What storage type are we modifying
*/
	void OnPickUp(class ATOMInteractables* Interactableptr, const FInteractables& InteractableInfo);


	/** Functions that adds the item info to the inventory array
*   @param InteractableInfo - Struct containing the intractable item info
*   @param Index - The array index where int interactable is inserted in
*   @param StorageType - What storage type are we are modifying
*/
	void AddItemToArray(const FInteractables& InteractableInfo, int32 Index);
};
