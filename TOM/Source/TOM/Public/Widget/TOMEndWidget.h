// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "TOMWidgetBase.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

/** Auto Generated Includes */
#include "TOMEndWidget.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API UTOMEndWidget : public UTOMWidgetBase
{
	GENERATED_BODY()

public:

	/** Sets default values for this component's properties */
	UTOMEndWidget(const FObjectInitializer& ObjectInitializer);

protected:

	/** Binds the Morning Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Exit;

	/** Sound cue ref*/
	class USoundCue* EndMusic;

public:


	/** Function to allow Widget class to set up what it needs */
	virtual bool Initialize() override;


	/** Function That cleans up widget when moved from the world*/
	void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;


	/** Function that adds the widget to the viewport, enabling the players to see it */
	virtual void Setup() override;


	/** Function that allows the widget class to do any clean up needed */
	virtual void CleanUp() override;


	/** Function that sets the input mode for a widget */
	virtual void SetInputMode(bool bActive) override;

	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
		void OnButtonClicked();
	
};
