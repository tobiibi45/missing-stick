// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "TOMWidgetBase.h"
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

/** Auto Generated Includes */
#include "TOMMainHUD.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API UTOMMainHUD : public UTOMWidgetBase
{
	GENERATED_BODY()
	
public:

	/** Sets default values for this component's properties */
	UTOMMainHUD(const FObjectInitializer& ObjectInitializer);

protected:

	/** Binds the Morning Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Text_TimeOfDay;


	/** Binds the Afternoon Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_Icon;


	/** Binds the Evening Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_Stick;


	/** Binds the Stick Map from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTOMStickMap* WBP_StickMap;


	/** Binds the Stick Map from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTOMDayClock* WBP_TimeDayClock;

	/** The Slate brush that controls the appearance of the texture*/
	UPROPERTY()
	FSlateBrush SlateBrush;


	/** Binds the Task Text info from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextBlock_TasksInfo;


	/** Binds the Task Text info from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextBlock_Objectives;

	/** bool that controls the objective at the beginning*/
	bool bFirstInteract = false;

	/** Timer to remove any last task info*/
	FTimerHandle TimerHandle_CkearTaskInfo;

	/** Counter that keeps track of how many map pieces found */
	UPROPERTY()
	int32 ProgressCounter = 0;


public:


	/** Function to allow Widget class to set up what it needs */
	virtual bool Initialize() override;


	/** Function That cleans up widget when moved from the world*/
	void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;


	/** Function that adds the widget to the viewport, enabling the players to see it */
	virtual void Setup() override;


	/** Function that allows the widget class to do any clean up needed */
	virtual void CleanUp() override;


	/** Function that sets the input mode for a widget */
	virtual void SetInputMode(bool bActive) override;


	/** Function that sets the image */
	void SetTimeDayImage(class UTexture2D* Image);

	/** Function that sets the text */
	void SetTimeDayText(ETimeOfDay TimeOfDay);

	/** Function that sets the text */
	void SetTaskInfoText(FString & Infostr, float length = 5.0f);

	/** Function that sets the objective text */
	void SetObjectiveInfoText(FString& Infostr);

	/** Function that sets the stick image to full opacity */
	void SetStickImage();

	/** Function that clears the task info text*/
	void OnClearTaskInfoText();

	/** Function that returns the map class*/
	class UTOMStickMap* GetMapRef();

	/** Function that returns the DayTime class*/
	class UTOMDayClock* GetDayTimeRef();

};
