// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "TOMWidgetBase.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

/** Auto Generated Includes */
#include "TOMStickMap.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API UTOMStickMap : public UTOMWidgetBase
{
	GENERATED_BODY()
	
public:

	/** Sets default values for this component's properties */
	UTOMStickMap(const FObjectInitializer& ObjectInitializer);

protected:

	/** Binds the Top Left image from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_TopLeft;


	/** Binds the Top right image from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_TopRight;


	/** Binds the Bottom Left image from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_BottomLeft;


	/** Binds the Bottom right image from the widget */
	UPROPERTY(meta = (BindWidget))
	class UImage* Image_BottomRight;


	/** Binds the percentage text from the widget */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextBlock_Percentage;


	/** Binds the close Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Close;


	/** Boolean for top left*/
	bool bTopLeft;


	/** Boolean for top right*/
	bool bTopRight;


	/** Boolean for bottom left*/
	bool bBottomLeft;


	/** Boolean for bottom right*/
	bool bBottomRight;


	/** Number that tracks how many task are completed*/
	int32 PercentageCounter;

	/** Number that tracks how many task are completed*/
	int32 PercentageCounterForMaps;

public:


	/** Function to allow Widget class to set up what it needs */
	virtual bool Initialize() override;


	/** Function That cleans up widget when moved from the world*/
	void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;


	/** Function that adds the widget to the viewport, enabling the players to see it */
	virtual void Setup() override;


	/** Function that allows the widget class to do any clean up needed */
	virtual void CleanUp() override;


	/** Function that sets the input mode for a widget */
	virtual void SetInputMode(bool bActive) override;


	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
	void OnCloseClicked();


	/** Function that sets the top left image */
	UFUNCTION()
	void SetTopLeft();


	/** Function that sets the top right image */
	UFUNCTION()
	void SetTopRight();


	/** Function that sets the bottom left image */
	UFUNCTION()
	void SetBottomLeft();


	/** Function that sets the bottom right image */
	UFUNCTION()
	void SetBottomRight();


	/** Function that sets the bottom right image */
	UFUNCTION()
	void SetPercentageText();


	/** Function that return true if map is completed */
	UFUNCTION()
	bool IsMapCompleted();


	/** Function that returns the percentage progress for the map*/
	int32 GetPercantage();
};
