// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "TOMWidgetBase.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

/** Auto Generated Includes */
#include "TOMDayClock.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API UTOMDayClock : public UTOMWidgetBase
{
	GENERATED_BODY()

public:

	/** Sets default values for this component's properties */
	UTOMDayClock(const FObjectInitializer& ObjectInitializer);

protected:

	/** Binds the Morning Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Morning;


	/** Binds the Afternoon Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Afternoon;


	/** Binds the Evening Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Evening;


	/** Binds the Evening Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_Close;


	/** Binds the Sound On button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_SoundOn;


	/** Binds the Sound off Button from the widget */
	UPROPERTY(meta = (BindWidget))
	class UButton* Button_SoundOff;


	/** Ptr to SkySphere Object */
	UPROPERTY()
	class ATOMSkySphere* TheSkySphere;

public:


	/** Function to allow Widget class to set up what it needs */
	virtual bool Initialize() override;


	/** Function That cleans up widget when moved from the world*/
	void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;


	/** Function that adds the widget to the viewport, enabling the players to see it */
	virtual void Setup() override;


	/** Function that allows the widget class to do any clean up needed */
	virtual void CleanUp() override;


	/** Function that sets the input mode for a widget */
	virtual void SetInputMode(bool bActive) override;


	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
	void OnMorningClicked();


	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
	void OnAfternoonClicked();


	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
	void OnEveningClicked();


	/** Function that gets called when the button click event is triggered */
	UFUNCTION()
	void OnCloseClicked();
	

	/** Function that gets called when the button Sound On Is Pressed */
	UFUNCTION()
	void OnSoundOnButtonClicked();


	/** Function that gets called when the button Sound On Is Pressed */
	UFUNCTION()
	void OnSoundOffButtonClicked();
};
