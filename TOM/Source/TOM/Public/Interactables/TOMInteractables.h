// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Game Includes*/
#include "StructsAndEnums/StructsAndEnums.h"

/**Engine Includes*/
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

/**Auto generated Includes*/
#include "TOMInteractables.generated.h"

UCLASS()
class TOM_API ATOMInteractables : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATOMInteractables();

protected:

	/** Pointer for the scene comp class */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactables", meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComp = nullptr;


	/** Mesh component for the actor */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactables", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComp = nullptr;

	/** Mesh component for the actor */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactables", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* OtherMeshComp = nullptr;


	/** Sphere component for the actor */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactables", meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SphereComp = nullptr;


	/** Pointer to the player controller */
	class ATOMPlayerController* PlayerController = nullptr;


	/** Pointer to the Character */
	class ATOMCharacter* Character = nullptr;


	/** Timer that puts the item to sleep after collision ends*/
	FTimerHandle TimerHandle_ItemToSleep;

	/** Is true if pawn is overlapping this interactable object otherwise false */
	bool bIsOverlapping = false;

	/** Is true its the first time player interacts with the clock */
	bool bFirstClockInteract = false;

	/** Struct that contains information about the interactable item */
	UPROPERTY(EditDefaultsOnly, Category = "Interactables", meta = (AllowPrivateAccess = "true"))
	FInteractables InteractableInfo;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/**
	 *	Event called when something starts to overlaps this component, for example a player walking into a trigger.
	 *	For events when objects have a blocking collision, for example a player hitting a wall, see 'Hit' events.
	 *
	 *	@note Both this component and the other one must have GetGenerateOverlapEvents() set to true to generate overlap events.
	 *	@note When receiving an overlap from another object's movement, the directions of 'Hit.Normal' and 'Hit.ImpactNormal'
	 *	will be adjusted to indicate force from the other object against this object.
	 */
	UFUNCTION()
		void OnInteractableBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	/**
	 *	Event called when something stops overlapping this component
	 *	@note Both this component and the other one must have GetGenerateOverlapEvents() set to true to generate overlap events.
	 */
	UFUNCTION()
		void OnInteractableEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	/**
	 *	Function that sets the owner of this interactable item
	 *  @param  OtherActor  Actor that is overlapping with this interactable item
	 */
	void SetInteractableOwner(AActor* OtherActor);


	/**
	 *	Function that resets the owner of this interactable item
	 *  @param  OtherActor  Actor that is overlapping with this interactable item
	 */
	void RestetInteractableOwner();


	/**
	 *	Function that sets the item to sleep when no player is close
	 */
	void SetItemToSleep();

public:	

	/** Returns the static mesh component **/
	FORCEINLINE class UStaticMeshComponent* GetSkeletalMesh() const { return MeshComp; }

	/** Functions that removes the item information data from the temp array when player stopped overlapping with the item
	*/
	UFUNCTION()
	void OnInteract();
};
