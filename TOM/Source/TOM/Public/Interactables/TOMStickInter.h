// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Engine Includes*/
#include "CoreMinimal.h"
#include "Interactables/TOMInteractables.h"

/**Auto generated Includes*/
#include "TOMStickInter.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API ATOMStickInter : public ATOMInteractables
{
	GENERATED_BODY()

public:

	// Sets default values for this actor's properties
	ATOMStickInter();
	
};
