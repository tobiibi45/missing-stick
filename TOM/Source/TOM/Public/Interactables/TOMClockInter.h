// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/**Engine Includes*/
#include "CoreMinimal.h"
#include "Interactables/TOMInteractables.h"

/**Auto generated Includes*/
#include "TOMClockInter.generated.h"

/**
 * 
 */
UCLASS()
class TOM_API ATOMClockInter : public ATOMInteractables
{
	GENERATED_BODY()

public:

	// Sets default values for this actor's properties
	ATOMClockInter();
	
};
