// Copyright 2019 Krankyz Stuidos, All Rights Reserved.

#pragma once

/** Game Includes */
#include "StructsAndEnums/StructsAndEnums.h"

/** Engine Includes */
#include "CoreMinimal.h"
#include "UObject/Interface.h"

/** Auto Generated Includes */
#include "TOMDayNightInt.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTOMDayNightInt : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TOM_API ITOMDayNightInt
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/** Function that sets the time of day using the interface
	*	@param TimeOfDay	The time of day you want to set it to. Morning, Afternoon or Evening
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TOM|DayTimeInterace")
	bool SetTimeOfDay(ETimeOfDay TimeOfDay);
};
